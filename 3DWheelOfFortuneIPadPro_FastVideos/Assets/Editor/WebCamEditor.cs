﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(webcamTexture))]


public class WebCamEditor : Editor {

   
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.BeginHorizontal();

        webcamTexture webcmTxtr = (webcamTexture)target;
        
        List<string> options = new List<string>();
        foreach (WebCamDevice device in WebCamTexture.devices)
            options.Add(device.name);

        int selected = EditorGUILayout.Popup("Camera", webcmTxtr.Camera, options.ToArray());
        webcmTxtr.Camera = selected;

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        string[] resolution =
        {
            "2160x3840",
            "1080x1920",
        };

        int resoSelect = EditorGUILayout.Popup("Resolution", webcmTxtr.selectedResolution, resolution);
        webcmTxtr.selectedResolution = resoSelect;

        webcmTxtr.width= int.Parse(resolution[resoSelect].Substring(0, 4));
        webcmTxtr.height = int.Parse(resolution[resoSelect].Substring(5, 4));

        GUILayout.EndHorizontal();
    }
}
