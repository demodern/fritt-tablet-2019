﻿using System;
using Amazon.S3.Model;
using AWSSDK.Examples;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace AWSSDK
{
    public class Uploader : MonoBehaviour
    {
        public string S3BucketName;
        public string baseURL = "http://www.superfritt-testsite.com.s3-website-us-east-1.amazonaws.com/?image=";

        public static event Action<string> UploadStarted;
        public static event Action<string> UploadDone;

        public void Start()
        {
            UploadDone += Debug.Log;
        }

        public void UploadObjectForBucket(string pathFile, string fileNameOnBucket)
        {
            // UploadStarted(baseURL + fileNameOnBucket);  
            UploadStarted( fileNameOnBucket);
            string uploadString = baseURL + fileNameOnBucket;
            Debug.Log("Uploading to: " + uploadString);
            S3Manager.Instance.UploadObjectForBucket(pathFile, S3BucketName, fileNameOnBucket, (result, error) =>
            {
                if (string.IsNullOrEmpty(error))
                {
                    UploadDone("Upload was a Success!");
                    // Application.OpenURL(baseURL + fileNameOnBucket);
                    if (File.Exists(pathFile))
                    {
                        File.Delete(pathFile);
                    }
                }
                else
                {
                    UploadDone("Upload Error: " + error);

                    if (File.Exists(pathFile))
                    {
                        File.Delete(pathFile);
                    }
                }
            });
        }
    }
}