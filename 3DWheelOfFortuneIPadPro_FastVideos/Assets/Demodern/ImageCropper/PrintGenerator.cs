﻿using AWSSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.UI;

public class PrintGenerator : MonoBehaviour
{

    public RenderTexture PrintTexture;

    public Camera PrintCam;
    public RawImage DebugImage;
    public RawImage PortaitImage;
    public ScreenShotManager ssm;
 

    public delegate void PrintIsReady();
    public PrintIsReady PrintReady;
    public ImageCropperPrint ic;

    void Start()
    {
        //Assign all Rendertextures to the Cameras
        InitRenderTextures();
       // ssm.PhotoIsDone += Uploader.uplo


    }



    private void InitRenderTextures()
    {
        PrintTexture = AssignRendertextureToCam(PrintCam);
        
    }

    private RenderTexture AssignRendertextureToCam(Camera cam)
    {
        if (cam.targetTexture != null)
            cam.targetTexture.Release();

        cam.targetTexture = new RenderTexture((int)(Screen.width ), (int)(Screen.height), 24);
        return cam.targetTexture;
    }

    //Call this from Button etc..
    public void GeneratePrintScreenshot()
    {
        PortaitImage.texture = StaticExample.UploadPhoto;

        if (PrintTexture == null)
            PrintTexture = AssignRendertextureToCam(PrintCam);
        StartCoroutine(TakeScreenshot());
    }

    IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        

        //var Screenshot = SaveScreenshot(PrintTexture);
        //Preview Screenshot
        //PC.SetTexture (Screenshot);
        //PC.ShowPolaroid ();
        //Texture2D PrintCropped = ic.CropTextureForPrint(Screenshot);
        //StaticExamplePrint.PrintTexture = PrintCropped;

        
        PrintReady?.Invoke();

    }

  

    public Texture2D SaveScreenshot(RenderTexture rt)
    {
        RenderTexture.active = rt;
        Texture2D screenshotTexture = new Texture2D(rt.width, rt.height, TextureFormat.RGB24, false);
        screenshotTexture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        screenshotTexture.Apply();
        /*
        // Encode texture into JPG
        byte[] bytes = screenshotTexture.EncodeToJPG();

        // For testing purposes, also write to a file in the project folder
        string generatedName = FileNameGenerator.GenerateFileName(ScreenshotName, ".jpg");
        string path = Application.persistentDataPath + "/" + generatedName;
        //path = "C:/FolderMill Data/Hot Folders/1/Incoming/" + generatedName;
        File.WriteAllBytes(path, bytes);
        Uploader.UploadObjectForBucket(path, generatedName);
        */
        return screenshotTexture;
    }

  
}
