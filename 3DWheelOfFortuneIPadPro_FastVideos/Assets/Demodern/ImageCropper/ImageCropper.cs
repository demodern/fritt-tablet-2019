﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageCropper : MonoBehaviour
{

    public Image ImageIn;
    public Image ImageOut;
    public RectTransform FramePosition;


    public enum RectOptions
    {
        Center = 0,
        BottomRight = 1,
        TopRight = 2,
        BottomLeft = 3,
        TopLeft = 4,
        //Top = 5,
        //Left = 6,
        //Right = 7,
        //Bottom = 8,
        Custom = 9
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("FramePosition: " + FramePosition.anchoredPosition.y);
    }


    public void CropImage()
    {
        Texture2D InputImageTex = (Texture2D)ImageIn.mainTexture;

        float centerX = 0f;
        float centerY = 2000f;
        float targetWidth = 1927f;
        float targetHeight = 1927f;

        Rect CropRect = new Rect(centerX, centerY, targetWidth, targetHeight);
        Texture2D result = CropWithRect(InputImageTex, CropRect);


        ImageOut.sprite = Sprite.Create(result, new Rect(0, 0, result.width, result.height), new Vector2(.5f, .5f));


    }

    public Texture2D CropTexturefromScreenshot(Texture2D texture) //// Work here
    {
        Texture2D InputImageTex = texture;

        float centerX = 0f;
        float centerY = 2000f;
        float targetWidth = 1920f; //1927
        float targetHeight = 1920f; //1927

        Rect CropRect = new Rect(centerX, centerY, targetWidth, targetHeight);
        Texture2D result = CropWithRect(InputImageTex, CropRect);

        return result;
        // ImageOut.sprite = Sprite.Create(result, new Rect(0, 0, result.width, result.height), new Vector2(.5f, .5f));

    }

    public Texture2D CropTextureForPrint(Texture2D texture)
    {
        Texture2D InputImageTex = texture;

        float centerX = 0f;
        float centerY = 1238;
        float targetWidth = 2160;
        float targetHeight = 2601;

        Rect CropRect = new Rect(centerX, centerY, targetWidth, targetHeight);
        Texture2D result = CropWithRectForPrint(InputImageTex, CropRect);

        return result;
        // ImageOut.sprite = Sprite.Create(result, new Rect(0, 0, result.width, result.height), new Vector2(.5f, .5f));

    }

    private  Texture2D CropWithRect(Texture2D texture, Rect r)
    {
        if (r.height < 0 || r.width < 0)
        {
            return texture;
        }
        Texture2D result = new Texture2D((int)r.width, (int)r.height);
        if (r.width > 0 && r.height > 0)
        {
            float xRect = r.x;
            float yRect = r.y;
            float widthRect = r.width;
            float heightRect = r.height;
            // center mode
            xRect = (texture.width - r.width) / 2;
            //yRect = 1493  ;
            yRect = 810 + FramePosition.anchoredPosition.y  ;



            if (texture.width < r.x + r.width || texture.height < r.y + r.height || xRect > r.x + texture.width || yRect > r.y + texture.height || xRect < 0 || yRect < 0 || r.width < 0 || r.height < 0)
            {

                Debug.Log("wrong size");
                //EditorUtility.DisplayDialog("Set value crop", "Set value crop (Width and Height > 0) less than origin texture size \n" + texture.name + " wrong size", "ReSet");
                //return texture;
            }
            result.SetPixels(texture.GetPixels(Mathf.FloorToInt(xRect), Mathf.FloorToInt(yRect), Mathf.FloorToInt(widthRect), Mathf.FloorToInt(heightRect)));
            result.Apply();
        }
        return result;
    }

    private static Texture2D CropWithRectForPrint(Texture2D texture, Rect r)
    {
        if (r.height < 0 || r.width < 0)
        {
            return texture;
        }
        Texture2D result = new Texture2D((int)r.width, (int)r.height);
        if (r.width > 0 && r.height > 0)
        {
            float xRect = r.x;
            float yRect = r.y;
            float widthRect = r.width;
            float heightRect = r.height;
            // center mode
            xRect = (texture.width - r.width) / 2;
            yRect = 1239;




            if (texture.width < r.x + r.width || texture.height < r.y + r.height || xRect > r.x + texture.width || yRect > r.y + texture.height || xRect < 0 || yRect < 0 || r.width < 0 || r.height < 0)
            {

                Debug.Log("wrong size");
                //EditorUtility.DisplayDialog("Set value crop", "Set value crop (Width and Height > 0) less than origin texture size \n" + texture.name + " wrong size", "ReSet");
                //return texture;
            }
            result.SetPixels(texture.GetPixels(Mathf.FloorToInt(xRect), Mathf.FloorToInt(yRect), Mathf.FloorToInt(widthRect), Mathf.FloorToInt(heightRect)));
            result.Apply();
        }
        return result;
    }


}
