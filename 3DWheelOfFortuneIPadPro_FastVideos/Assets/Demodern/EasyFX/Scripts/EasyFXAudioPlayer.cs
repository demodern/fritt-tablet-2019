﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyFXAudioPlayer : MonoBehaviour {

    public EasyFXAudio ButtonFXAudio;
    EasyFXAudio AddedAudioFX;
    // Use this for initialization
    void Start()
    {
        if ( ButtonFXAudio != null)
        {
            AddedAudioFX = gameObject.AddComponent<EasyFXAudio>(ButtonFXAudio);
            // tmpAudioFX.SetDebugUIState(false);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void PlayAudioClip()
    {
        if (AddedAudioFX != null)
        {
            AddedAudioFX.PlayAudioClip();
            // tmpAudioFX.SetDebugUIState(false);
        }
    }
}
