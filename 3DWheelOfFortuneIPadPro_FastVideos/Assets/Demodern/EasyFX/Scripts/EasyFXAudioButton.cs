﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EasyFXAudioButton : MonoBehaviour {
    public EasyFXAudio ButtonFXAudio;
    EasyFXAudio AddedAudioFX;
    // Use this for initialization
    void Start () {
        if (GetComponent<Button>() != null && ButtonFXAudio != null)
        {
            GetComponent<Button>().onClick.AddListener(delegate { PlayAudioClip(); });
            AddedAudioFX = gameObject.AddComponent<EasyFXAudio>(ButtonFXAudio);
           // tmpAudioFX.SetDebugUIState(false);
        }

        if (GetComponent<Toggle>() != null && ButtonFXAudio != null)
        {
            GetComponent<Toggle>().onValueChanged.AddListener(delegate { PlayAudioClip(); });
            AddedAudioFX = gameObject.AddComponent<EasyFXAudio>(ButtonFXAudio);
            // tmpAudioFX.SetDebugUIState(false);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void PlayAudioClip() {
        AddedAudioFX.PlayAudioClip();
    }


}
