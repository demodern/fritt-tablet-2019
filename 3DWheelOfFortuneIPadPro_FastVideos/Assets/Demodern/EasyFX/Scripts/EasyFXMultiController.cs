﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyFXMultiController : MonoBehaviour {

    public EasyFXController[] EasyFXControllers;
    public EasyFXMultiController[] SubmenusToStart;
    public bool ShowDebugGUI;
    public EasyFXMultiController[] NextMenusToPlay;
    public int NextMenuIndex = 0;
    public bool ThisIsTheStartMenu = false;
    public bool PlayNextMenuAutomatic = false;
    public float PlayNextDuration = 4f;

    float CurrAutoNextTime = 0f;
    bool AutoNextIsRunning = false;

    int currentEndAnimationFinishedIndex = 0;


    // delegates
    public delegate void StartIsTriggered();
    public StartIsTriggered StartTriggered;

    public delegate void EndIsTriggered();
    public EndIsTriggered EndTriggered;

    public delegate void MenuEndIsFinished();
    public MenuEndIsFinished MenuEndFinished;

    // Use this for initialization
    void Start() {
        InitCallbacks();
        InitGUIs();

        Transform t = transform.GetChild(0);


        if (ThisIsTheStartMenu && !ShowDebugGUI)
        {
            Debug.Log("should play automatically");
            ActivateAutostart();
        }
    }

    // Update is called once per frame
    void Update() {
        CheckAutoNext();
    }

    private void CheckAutoNext()
    {
        if (AutoNextIsRunning)
        {
            CurrAutoNextTime += Time.deltaTime;
            if (CurrAutoNextTime >= PlayNextDuration)
            {
                AutoNextIsRunning = false;
                PlayEnd();
            }
        }
    }

    void ActivateAutostart()
    {
        foreach (EasyFXController efxc in EasyFXControllers)
        {
            //efxc.PlayStartOnLoad = true;
            efxc.PlayStart();
        }
        foreach (EasyFXMultiController efxc in SubmenusToStart)
        {
            efxc.PlayStart();
        }
    }

    void InitCallbacks()
    {
        foreach (EasyFXController efxc in EasyFXControllers)
        {
            efxc.SetDebugGuiInvisible();
            efxc.EndAnimationFinished += CheckForFinishedState;
        }
    }

    void InitGUIs()
    {
        foreach (EasyFXController efxc in EasyFXControllers)
        {

            efxc.ShowDebugGUI = false;
        }


        Transform t = transform.GetChild(0);

        if (t != null)
        {
            if (ShowDebugGUI)
            {
                t.gameObject.SetActive(true);
            }
            else
            {
                t.gameObject.SetActive(false);
            }
        }
    }

    public void PlayStart()
    {
        InitGUIs();
        CurrAutoNextTime = 0f;
        if (StartTriggered != null)
        {
            StartTriggered();
        }


        if (PlayNextMenuAutomatic)
        {
            AutoNextIsRunning = true;
        }
        foreach (EasyFXController efxc in EasyFXControllers)
        {
            efxc.PlayStart();
        }

        foreach (EasyFXMultiController efxc in SubmenusToStart)
        {
            efxc.PlayStart();
        }


    }

    public void PlayEnd()
    {

        if (EndTriggered != null)
        {
            EndTriggered();
        }

        foreach (EasyFXController efxc in EasyFXControllers)
        {
            efxc.PlayEnd();
        }

        foreach (EasyFXMultiController efxc in SubmenusToStart)
        {
            efxc.PlayEnd();
        }
    }

    void CheckForFinishedState(int CallbackObjectID)
    {
        currentEndAnimationFinishedIndex++;

        if (currentEndAnimationFinishedIndex == EasyFXControllers.Length)
        {
            currentEndAnimationFinishedIndex = 0;
            ShowNextMenu();
            if (MenuEndFinished != null)
            {
                MenuEndFinished();
            }

        }
    }

    public void ShowNextMenuWithIndex(int index)
    {

        Debug.Log("ShowNextMenuWithIndex called");
        NextMenuIndex = index;
        ShowNextMenu();

    }

    void ShowNextMenu()
    {
        Transform t = transform.GetChild(0);
        if (t != null)
        { 
            t.gameObject.SetActive(false);
        }

        foreach (EasyFXController efxc in EasyFXControllers)
        {
            efxc.SetDebugGuiInvisible();
        }

        if (NextMenusToPlay.Length >0  && NextMenusToPlay[NextMenuIndex] != null) {
            NextMenusToPlay[NextMenuIndex].gameObject.SetActive(true);
            gameObject.SetActive(false);
            NextMenusToPlay[NextMenuIndex].PlayStart();
        }    
    }


  
}
