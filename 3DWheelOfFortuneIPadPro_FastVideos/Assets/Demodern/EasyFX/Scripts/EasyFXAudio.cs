﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class EasyFXAudio : MonoBehaviour
{
    AudioSource audioSourse;
    public AudioClip audioClip;
    Canvas DebugUI;
    // Use this for initialization
    void Start()
    {
        
        if (audioClip != null)
        {
            SetupAudioComponent(audioClip);
        }
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void PlayAudioClip()
    {
        //Debug.Log("PlayClickSound called by:" + gameObject.name);
        if (audioSourse != null && audioClip != null)
        {
            audioSourse.Play();
        }
        
    }

    public void SetupAudioComponent(AudioClip audioCl)
    {
        if (audioSourse == null)
        {

        audioSourse = gameObject.AddComponent<AudioSource>();
        }

        audioSourse.clip = audioCl;
        audioSourse.playOnAwake = false;
    }

    public void SetDebugUIState(bool isVisible)
    {


        if (transform.childCount > 0)
        {
            Transform t = transform.GetChild(0);

            if (t != null)
            {
                t.gameObject.SetActive(isVisible);
            }
        }
    }
}
