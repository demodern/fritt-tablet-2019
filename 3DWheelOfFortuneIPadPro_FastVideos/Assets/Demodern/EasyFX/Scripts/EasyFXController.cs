﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
//using UnityEditorInternal;

public class EasyFXController : MonoBehaviour {

    public GameObject[] ObjectsToAnimate;
    public float AnimationOffsetPerObject = 0.5f;
    public bool AllowIdleLoop = false;
    public bool RandomizeIdleLoop = false;
    public EasyFXConfiguration ConfigurationStart;  
    public EasyFXConfiguration ConfigurationIdle;
    public EasyFXConfiguration ConfigurationEnd;
    public EasyFXAudio AudioFXStart;
    public EasyFXAudio AudioFXIdle;
    public EasyFXAudio AudioFXEnd;

    public float TimeOffsetToStart = 0;
    public float TimeOffsetToEnd = 0;

    public bool PlayStartOnLoad;
    public bool ShowDebugGUI;

    // delegates
    public delegate void MenuAnimationsFinished(int id);
    public MenuAnimationsFinished EndAnimationFinished;

    List<EasyFXConfiguration> Starts;
    List<EasyFXConfiguration> Idles;
    List<EasyFXConfiguration> Ends;


    int numOfFinishedEndAnimations = 0;

    bool ClassIsInitialized = false;
    public int numOfStarts;
   


    // Use this for initialization 
    void Start () {

        InitClass();
    }


    public void InitClass()
    {
        if (!ClassIsInitialized)
        {

        Starts = new List<EasyFXConfiguration>();
        Idles = new List<EasyFXConfiguration>();
        Ends = new List<EasyFXConfiguration>();

 

            InitDebugGUI();

        CreateComponentsInObjectsToAnimate();

        SetActiveStateOfConfigurations(false);
        if (PlayStartOnLoad)
        {
            PlayStart();
        }
            ClassIsInitialized = true;
    }
    }


    void SetActiveStateOfConfigurations(bool state)
    {
        foreach (EasyFXConfiguration ec in Starts)
        {
            ec.gameObject.SetActive(state);
        }
        foreach (EasyFXConfiguration ec in Idles)
        {
            ec.gameObject.SetActive(state);
        }
        foreach (EasyFXConfiguration ec in Ends)
        {
            ec.gameObject.SetActive(state);
        }
    }



    void InitDebugGUI()
    {
        if (ShowDebugGUI)
        {
            SetDebugGuiVisible();
        }
        else
        {
            SetDebugGuiInvisible();
        }
    }


    public void SetDebugGuiVisible()
    {
        if (transform.childCount  >0) { 
        Transform t = transform.GetChild(0);

        if (t != null) {
            t.gameObject.SetActive(true);
        }
        ShowDebugGUI = true;
    }
    }


    public void SetDebugGuiInvisible()
    {
        Transform t = transform.GetChild(0);

        if (t != null)
        {
            t.gameObject.SetActive(false);
        }
        ShowDebugGUI = false;
    }


    void CreateComponentsInObjectsToAnimate(){

        for (int i = 0; i < ObjectsToAnimate.Length; i++)
        {
            if (ConfigurationStart != null) { 
               // EasyFXConfiguration start = ObjectsToAnimate[i].AddComponent<EasyFXConfiguration>();
               //ComponentUtility.CopyComponent(ConfigurationStart);
               // ComponentUtility.PasteComponentValues(start);
               
               
                EasyFXConfiguration start = ObjectsToAnimate[i].AddComponent<EasyFXConfiguration>(ConfigurationStart);
                //start.Add<EasyFXConfiguration>(otherGameObject.GetComponent<EasyFXConfiguration>());
                start.isAllowedToPlay = false;
                start.AllowLoop = false;
                start.AnimationObjectID = i;
                start.SetShowFirstFrameOnStart(true);
                start.InitialzeValues();
                start.IsControoledByController = true;
                start.AnimDidFinish += StartAnimationFinished;
                start.SetOnceMode();
                if (AudioFXStart != null)
                {
                    EasyFXAudio startAudio = ObjectsToAnimate[i].AddComponent<EasyFXAudio>(AudioFXStart);
                    startAudio.SetDebugUIState(false);
                    start.SetEasyFXAudio(startAudio);
                }

                Starts.Add(start);
                numOfStarts = Starts.Count;
             }

            if (ConfigurationIdle != null){
                //EasyFXConfiguration idle = ObjectsToAnimate[i].AddComponent<EasyFXConfiguration>();
                //ComponentUtility.CopyComponent(ConfigurationIdle);
                //ComponentUtility.PasteComponentValues(idle);
               

                EasyFXConfiguration idle = ObjectsToAnimate[i].AddComponent<EasyFXConfiguration>(ConfigurationIdle);
                idle.isAllowedToPlay = false;
                idle.AllowLoop = AllowIdleLoop;
                idle.InitialzeValues();
                idle.AnimationObjectID = i;
                idle.IsControoledByController = true;
                idle.SetLoopMode();
                if (AudioFXIdle != null)
                {
                    EasyFXAudio idleAudio = ObjectsToAnimate[i].AddComponent<EasyFXAudio>(AudioFXIdle);
                    idleAudio.SetDebugUIState(false);
                    idle.SetEasyFXAudio(idleAudio);
                }

                
                Idles.Add(idle);
            }
            if (ConfigurationEnd != null){
                //EasyFXConfiguration end = ObjectsToAnimate[i].AddComponent<EasyFXConfiguration>();
                //ComponentUtility.CopyComponent(ConfigurationEnd);
                //ComponentUtility.PasteComponentValues(end);

                

                EasyFXConfiguration end = ObjectsToAnimate[i].AddComponent<EasyFXConfiguration>(ConfigurationEnd);

                end.isAllowedToPlay = false;
                end.AllowLoop = false;
                end.AnimationObjectID = i;
                end.InitialzeValues();
                end.IsControoledByController = true;
                end.AnimStarting += StopIdleAnimation;
                end.AnimDidFinish += MenuAnimationFinished;
                end.SetOnceMode();
                if (AudioFXEnd != null)
                {
                    EasyFXAudio endAudio = ObjectsToAnimate[i].AddComponent<EasyFXAudio>(AudioFXEnd);
                    endAudio.SetDebugUIState(false);
                    end.SetEasyFXAudio(endAudio);
                }

                Ends.Add(end);
            }
        }
    }


    
    // Update is called once per frame
    void Update () {
        
    }


    public void PlayStart(){
        InitClass();
        //Debug.Log("play start called");
        SetActiveStateOfConfigurations(true);
        EasyFXConfiguration[] tmpStarts = Starts.ToArray();
       // Debug.Log("num of starts: " + tmpStarts.Length);
       
        for (int i = 0; i < tmpStarts.Length; i++){

            tmpStarts[i].StartAnimationWithOffset(-i * AnimationOffsetPerObject- TimeOffsetToStart);      
        }


        EasyFXConfiguration[] tmpEnds = Ends.ToArray();

        for (int i = 0; i < tmpEnds.Length; i++)
        {
            tmpEnds[i].isAllowedToPlay = false;
        }

        EasyFXConfiguration[] tmpIdles = Idles.ToArray();

        for (int i = 0; i < tmpIdles.Length; i++)
        {
            tmpIdles[i].isAllowedToPlay = false;
        }



    }

    void StartAnimationFinished(int ObjectID){
        //Debug.Log("StartAnimationFinished");
        EasyFXConfiguration[] starts = Starts.ToArray();
        EasyFXConfiguration[] idles;
        if (Idles != null)
        {
            idles = Idles.ToArray();


            starts[ObjectID].isAllowedToPlay = false;

            float Offset = 0;
            if (idles[ObjectID] != null)
            {
                if (RandomizeIdleLoop)
                {
                    Offset = Random.Range(0, idles[ObjectID].AnimationDuration);
                }
                idles[ObjectID].StartAnimationWithOffset(-Offset);
                //idles[ObjectID].
            }
        }
    }


    void MenuAnimationFinished(int ObjectID)
    {
        // Debug.Log("EndAnimationFinished");
        numOfFinishedEndAnimations++;
        if (EndAnimationFinished != null && numOfFinishedEndAnimations == Ends.Count)
        {
            EndAnimationFinished(ObjectID);
            //Debug.Log("MenuAnimationFinished called");
            SetActiveStateOfConfigurations(false);
        }

     
    }


    public void PlayEnd()
    {
        //Debug.Log("play end called");
        numOfFinishedEndAnimations = 0;
        // EasyFXConfiguration[] tmpIdles = Idles.ToArray();

        EasyFXConfiguration[] tmpEnds = Ends.ToArray();

        for (int i = 0; i < tmpEnds.Length; i++)
        {
            tmpEnds[i].StartAnimationWithOffset(-i * AnimationOffsetPerObject- TimeOffsetToEnd);
         }
    }


    void StopIdleAnimation(int IdleID){
   
        EasyFXConfiguration[] tmpIdles = Idles.ToArray();
        if (tmpIdles[IdleID] != null)
        {
            tmpIdles[IdleID].isAllowedToPlay = false;
        }
    }
}
