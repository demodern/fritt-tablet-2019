﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EasyFXConfiguration : MonoBehaviour {



    public float AnimationDuration = 1f;
    public bool AllowLoop = false;

    bool UseScale;
    public AnimationCurve ScaleX;
    public AnimationCurve ScaleY;
    public AnimationCurve ScaleZ;

    bool UseRotation;
    public float RotationScaler = 1f;
    public AnimationCurve RotationX;
    public AnimationCurve RotationY;
    public AnimationCurve RotationZ;

    bool UseTranslation;
    public float TranslationScaler = 1f;
    public AnimationCurve TranslationX;
    public AnimationCurve TranslationY;
    public AnimationCurve TranslationZ;

    public AnimationCurve Visibility;

    bool UseTransormations;
    public float currentTime;
    CanvasGroup cg;
    float prevTime;
    Vector3 initialScale;
    Vector3 initialRotation;
    Vector3 initialTranslation;
    public bool showDebug = false;
    float targetAlpha;
    EasyFXAudio easyAudio;

  

    // delegates
    public delegate void AnimationDidFinish(int id);
    public AnimationDidFinish AnimDidFinish;

    public delegate void AnimationIsStarting(int id);
    public AnimationIsStarting AnimStarting;

    public bool isAllowedToPlay = true;
    public int AnimationObjectID;

    bool ShowFirstFrameOnStart = false;

    public bool IsControoledByController = false;

    // Use this for initialization
    void Start () {
        // InitialzeValues();
        DetermineColorManipulation();
        prevTime = - 1f;
      
        if (!IsControoledByController)
        {
            InitialzeValues();
            StartAnimationWithOffset(0);
            UseTransormations = true;
        }
        else
        {
            UseTransormations = false;
            if (ShowFirstFrameOnStart) ShowFirstFrame();
        }

        if (AllowLoop) {
            SetLoopMode();
        }
        else
        {
            SetOnceMode();
        }


    }


    public void SetLoopMode()
    {
     ScaleX.postWrapMode = WrapMode.Loop;
     ScaleY.postWrapMode = WrapMode.Loop;
     ScaleZ.postWrapMode = WrapMode.Loop;

        RotationX.postWrapMode = WrapMode.Loop;
        RotationY.postWrapMode = WrapMode.Loop;
        RotationZ.postWrapMode = WrapMode.Loop;


        TranslationX.postWrapMode = WrapMode.Loop;
        TranslationY.postWrapMode = WrapMode.Loop;
        TranslationZ.postWrapMode = WrapMode.Loop;
    }

    public void SetOnceMode()
    {
        ScaleX.postWrapMode = WrapMode.ClampForever;
        ScaleY.postWrapMode = WrapMode.ClampForever;
        ScaleZ.postWrapMode = WrapMode.ClampForever;

        RotationX.postWrapMode = WrapMode.ClampForever;
        RotationY.postWrapMode = WrapMode.ClampForever;
        RotationZ.postWrapMode = WrapMode.ClampForever;


        TranslationX.postWrapMode = WrapMode.ClampForever;
        TranslationY.postWrapMode = WrapMode.ClampForever;
        TranslationZ.postWrapMode = WrapMode.ClampForever;
    }


    void DetermineColorManipulation()
    {
        if (GetComponent<Image>() != null || GetComponent<Toggle>() != null)
        {
            if (GetComponent<CanvasGroup>() == null )
            {
                cg = gameObject.AddComponent<CanvasGroup>();

                if (gameObject.GetComponent<Button>() == null && gameObject.GetComponent<Toggle>() == null)
                {
                    cg.blocksRaycasts = false;
                }

            }
            else
            {
                cg = gameObject.GetComponent<CanvasGroup>();
            }

        }
    }

    public void SetShowFirstFrameOnStart(bool show)
    {
        ShowFirstFrameOnStart = show;
    }


    // Update is called once per frame
    void Update () {

        if (AnimationDuration> 0f && isAllowedToPlay)
        {
            currentTime += Time.deltaTime / AnimationDuration;
          
            CheckStartOfAnimation();

           
            if (!AllowLoop && currentTime>=1f)
            {


                currentTime = 1;
                isAllowedToPlay = false;
                //Debug.Log(AllowLoop + " " + currentTime);

              

                //ShowFrameAtTime(0.999f);

                if (AnimDidFinish != null)
                {
                    AnimDidFinish(AnimationObjectID);
                }
                ShowFrameAtTime(0.999f);
                return;
            }

            if (currentTime <= 1f || AllowLoop)
            {

                if (UseTransormations)
                {

                    ShowFrameAtTime(currentTime);
                    /* UpdateScale();
                     UpdateRotation();
                     UpdateTranslation();
                     UpdateColor();*/
                }

            }

            if (prevTime <0f && currentTime>0f)
            {
                if (easyAudio != null)
                {
                    easyAudio.PlayAudioClip();
                }
            }

            prevTime = currentTime;
           
           
        }

      
    }

    public void CheckStartOfAnimation(){
       
        if (prevTime <0f && currentTime >= 0f)
        {
            UseTransormations = true;
            isAllowedToPlay = true;

            if (AnimStarting != null){
                AnimStarting(AnimationObjectID);
            }
        }
        if (currentTime < 0f)
        {
            UseTransormations = false;
            //isAllowedToPlay = false;
        }
    }

    public void SetEasyFXAudio(EasyFXAudio efa)
    {
        easyAudio = efa;
    }
  
    public void StartAnimation()
    {
        StartAnimationWithOffset(0f);
    }

    public void StartAnimationWithOffset(float TimeOffset)
    {
        prevTime = -1f;
        isAllowedToPlay = true;
        currentTime = TimeOffset/ AnimationDuration;
    }

    public void ShowFirstFrame()
    {
        ShowFrameAtTime(0f);
    }


    void ShowFrameAtTime(float time)
    {

       // Debug.Log("showframeattime: " + time);

        float tmpTime = time;
        transform.localScale = new Vector3(initialScale.x * ScaleX.Evaluate(tmpTime), initialScale.y * ScaleY.Evaluate(tmpTime), initialScale.z * ScaleZ.Evaluate(tmpTime));
        transform.localEulerAngles = new Vector3(initialRotation.x + RotationScaler * RotationX.Evaluate(tmpTime), initialRotation.y + RotationScaler * RotationY.Evaluate(tmpTime), initialRotation.z + RotationScaler * RotationZ.Evaluate(tmpTime));
        transform.localPosition = new Vector3(initialTranslation.x + TranslationScaler * TranslationX.Evaluate(tmpTime), initialTranslation.y + TranslationScaler * TranslationY.Evaluate(tmpTime), initialTranslation.z + TranslationScaler * TranslationZ.Evaluate(tmpTime));

        cg = gameObject.GetComponent<CanvasGroup>();
        if (cg != null)
        {
            targetAlpha = Visibility.Evaluate(tmpTime);
            if (targetAlpha >0.1) {
                cg.alpha = Visibility.Evaluate(tmpTime);
            }
            else
            {
                cg.alpha = 0f;
            }
            //Debug.Log("Visibility.Evaluate(currentTime): " + Visibility.Evaluate(currentTime) + " " + currentTime);
        }
    }

    public void InitialzeValues()
    {
        initialScale = transform.localScale;
        initialRotation = transform.localEulerAngles;
        initialTranslation = transform.localPosition;
     }

 
}
