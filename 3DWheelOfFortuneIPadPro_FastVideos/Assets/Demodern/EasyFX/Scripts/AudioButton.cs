﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AudioButton : MonoBehaviour {
    AudioSource audioSourse;
    AudioClip ac;
    // Use this for initialization
    void Start () {
        if (GetComponent<Button>() != null)
        {
            GetComponent<Button>().onClick.AddListener(delegate { PlayAudioClip(); });
        }
        ac = Resources.Load<AudioClip>("Button/ES_Drip Small 2 - SFX Producer");
        SetupAudioComponent(ac);
    } 

	
	// Update is called once per frame
	void Update () {
		
	}

    void PlayAudioClip()
    {
        //Debug.Log("PlayClickSound called by:" + gameObject.name);
        audioSourse.Play();
    }

    public void SetAudioClip()
    {

    }

    void SetupAudioComponent(AudioClip audioCl)
    {
        audioSourse = gameObject.AddComponent<AudioSource>();
        
        audioSourse.clip = audioCl;
        audioSourse.playOnAwake = false;
    }
}
