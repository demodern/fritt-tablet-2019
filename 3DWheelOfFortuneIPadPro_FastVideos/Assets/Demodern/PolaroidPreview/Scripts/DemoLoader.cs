﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoLoader : MonoBehaviour {

	public PolaroidController PC;
	public Texture2D DemoTexture;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetTexture(){
		PC.SetTexture (DemoTexture);
	}


	public void HidePolaroid(){
		PC.HidePolaroid ();
	}

	public void ShowPolaroid(){
		PC.ShowPolaroid ();
	}
}
