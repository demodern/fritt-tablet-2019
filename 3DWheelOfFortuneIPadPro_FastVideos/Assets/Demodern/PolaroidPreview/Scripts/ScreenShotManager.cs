﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using AWSSDK;
using AWSSDK.Examples;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShotManager : MonoBehaviour
{
    [Header("Settings")]
    public string ScreenshotName;
    public float FotoResolutionScaler = 2f;

    [Header("References")]
    public PolaroidController PC;
    public Uploader Uploader;
    //Rendertexture of Webcam and Visage AR Overlay
    public RenderTexture CamTexture;
    public RenderTexture AugmentedObjectsTexture;
    //Output of Rendertextures is stored in RawImage
    public RawImage CamImage;
    public RawImage AugmentedObjectsImage;
    public Camera CamImageCam;
    public Camera AugmentedObjectsCam;
    //Camera that Combines Webcam and AR Overlay
    public Camera FinalCompCam;
    public GameObject ARElements;
    public GameObject[] UIElementsToBlendOut;

    //Private Members
    private RenderTexture FinalCompTexture;
    private RenderTexture buffer;
    public delegate void PhotoIsTaken();
    public PhotoIsTaken PhotoIsDone;
    public ImageCropper ic;

    public PrintGenerator pg;

    string generatedName;
    void Start()
    {
        //Assign all Rendertextures to the Cameras
        InitRenderTextures();

        //Visualize all Rendertextures on RawImages
        AugmentedObjectsImage.texture = AugmentedObjectsTexture;
        CamImage.texture = CamTexture;
        pg.PrintReady += GenreatePrintAfterUpload;
    }

    private void InitRenderTextures()
    {
        FinalCompTexture = AssignRendertextureToCam(FinalCompCam);
        CamTexture = AssignRendertextureToCam(CamImageCam);
        AugmentedObjectsTexture =  AssignRendertextureToCam(AugmentedObjectsCam);
    }

    private RenderTexture AssignRendertextureToCam(Camera cam){
        if (cam.targetTexture != null)
            cam.targetTexture.Release();

        cam.targetTexture = new RenderTexture((int)(Screen.width * FotoResolutionScaler), (int)(Screen.height * FotoResolutionScaler), 24);
        return cam.targetTexture;
    }

    //Call this from Button etc..
	public void Photo()
	{
        Debug.Log("Photo called");
        if (FinalCompTexture == null)
            FinalCompTexture = AssignRendertextureToCam(FinalCompCam);
		StartCoroutine(TakeScreenshot());
	}   

    IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();

        SetUIVisibility(false);
        ARElements.SetActive (false);

       // var Screenshot = SaveScreenshot(FinalCompTexture);
        var Screenshot = ScreenCapture.CaptureScreenshotAsTexture();
        //Preview Screenshot
        //PC.SetTexture (Screenshot);
        //PC.ShowPolaroid ();
        StaticExample.Photo = Screenshot;
        Texture2D CroppedImage = ic.CropTexturefromScreenshot(StaticExample.Photo);
        StaticExample.UploadPhoto = CroppedImage;
        CamImage.texture = CroppedImage;
        UploadAndSaveOnDisk();
        //PhotoIsDone?.Invoke();
        SetUIVisibility(true);
    }

    public void UploadAndSaveOnDisk()
    {
      
       // Texture2D screenshotTexture = StaticExamplePrint.PrintTexture;
        
        // Encode texture into JPG
        //byte[] bytes = screenshotTexture.EncodeToJPG();
        byte[] bytesuploadImage = StaticExample.UploadPhoto.EncodeToJPG();

        // For testing purposes, also write to a file in the project folder

         generatedName = FileNameGenerator.GenerateFileName(ScreenshotName, ".jpg");
        string path = Application.persistentDataPath + "/" + generatedName;
        File.WriteAllBytes(path, bytesuploadImage);

        Uploader.UploadObjectForBucket(path, generatedName);
        PhotoIsDone?.Invoke();

    }
    public void GenreatePrintAfterUpload()
    {
        Texture2D screenshotTexture = StaticExamplePrint.PrintTexture;
        byte[] bytes = screenshotTexture.EncodeToJPG();
        // string path = Application.persistentDataPath + "/" + generatedName;
        string path = "C:/FolderMill Data/Hot Folders/1/Incoming/" + generatedName;
        //Debug.Log("Printing now!!!!!!!!");
        //File.WriteAllBytes(path, bytes);
        //Debug.Log("Printing now!!!!!!!!");
        CamImage.texture = screenshotTexture;
    }


        public Texture2D SaveScreenshot(RenderTexture rt)
    {
        RenderTexture.active = rt;
        Texture2D screenshotTexture = new Texture2D(rt.width, rt.height, TextureFormat.RGB24, false);
        screenshotTexture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        screenshotTexture.Apply();
        /*
        // Encode texture into JPG
        byte[] bytes = screenshotTexture.EncodeToJPG();

        // For testing purposes, also write to a file in the project folder
        string generatedName = FileNameGenerator.GenerateFileName(ScreenshotName, ".jpg");
        string path = Application.persistentDataPath + "/" + generatedName;
        //path = "C:/FolderMill Data/Hot Folders/1/Incoming/" + generatedName;
        File.WriteAllBytes(path, bytes);
        Uploader.UploadObjectForBucket(path, generatedName);
        */
        return screenshotTexture;
    }

    void SetUIVisibility(bool visibleState){
        for (int i = 0; i < UIElementsToBlendOut.Length; i++)
        {
            UIElementsToBlendOut[i].SetActive(visibleState);
        }
    }
}
