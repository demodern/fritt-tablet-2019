﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolaroidAutoMover : MonoBehaviour {

	public float speedX = 10f;
	public float speedY = 10f;
	public float speedZ = 10f;

	public float MaxX = 10f;
	public float MaxY = 10f;
	public float MaxZ = 10f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.localEulerAngles = new Vector3 (Mathf.Cos(speedX * Time.time)*MaxX, Mathf.Sin(speedY * Time.time)*MaxY, Mathf.Cos(speedZ * Time.time)*MaxZ);
	}
}
