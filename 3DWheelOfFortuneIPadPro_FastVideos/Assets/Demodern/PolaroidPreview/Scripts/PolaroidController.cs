﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PolaroidController : MonoBehaviour
{
	// Transforms to act as start and end markers for the journey.
	public Transform startMarker;
	public Transform endMarker;
	public GameObject Polaroid;
	public RawImage RawFotoImage;
	public GameObject MaskContainer;



	void Start()
	{
		HidePolaroid ();
	}


	void Update(){
		
	}



	public void HidePolaroid(){
		SetTargetTransform (startMarker);
	}

	public void ShowPolaroid(){
		SetTargetTransform (endMarker);
	}

	void SetTargetTransform(Transform target){
		Polaroid.GetComponent<CD_PositionSmoother> ().target = target;
		Polaroid.GetComponent<CD_RotationSmoother> ().target = target;
	}

	public void SetTexture(Texture2D t2D){
		RawFotoImage.texture = t2D;
		//MaskContainer.SetActive (false);
	}
}