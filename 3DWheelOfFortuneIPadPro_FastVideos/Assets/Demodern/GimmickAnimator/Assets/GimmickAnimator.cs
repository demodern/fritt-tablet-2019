﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GimmickAnimator : MonoBehaviour {


    public GameObject TargetObject;
    public GameObject SourcePosition;
    public Vector2 TargetObjectOffset;
    public GameObject GimmickImage;
    public float MoveSpeed = 1f;
    public float RotationPulseDuration = 1f;
    public AnimationCurve xRot;
    public AnimationCurve yRot;
    public AnimationCurve zRot;
    public float AnimationScale = 5;


    float currentTime;
    Vector3 currentImageGimmickEuler;
    Vector3 GimmickTargetPos;
    Vector3 InitialGimmickRotation;
    public GameObject InteractionArea;
    Rect IntaractionRect;

    float xMin;
    float xMax;
    float yMin;
    float yMax;



    // Use this for initialization
    void Start () {
        Debug.Log("Start called;");
        currentTime = 0f;
        InitialGimmickRotation = GimmickImage.transform.localEulerAngles;
        SourcePosition.transform.SetParent(null);
        gameObject.GetComponent<MeshRenderer>().enabled = false;

        IntaractionRect = InteractionArea.GetComponent<RectTransform>().rect;
        Debug.Log("IntaractionRect: " + IntaractionRect);
        Debug.Log("GimmickTargetPos: " + Camera.main.WorldToScreenPoint(TargetObject.transform.position));
        Vector3[] v = new Vector3[4];
        InteractionArea.GetComponent<RectTransform>().GetWorldCorners(v);

            Debug.Log("World Corners");
            for (var i = 0; i < 4; i++)
            {
               // Debug.Log("World Corner " + i + " : " + v[i]);
            }
        xMin = v[0].x;
        xMax = v[2].x;

        yMin = v[0].y;
        yMax = v[1].y;
        InteractionArea.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        GetTargetObjectScreenPos();
        UpdateRotation(); 
        UpdatePosition();
    }

    void GetTargetObjectScreenPos(){

        Vector3 targetObjectPos;
     
        if (TargetObject.activeInHierarchy == true)
        {
            targetObjectPos = Camera.main.WorldToScreenPoint(TargetObject.transform.position);
           
           
            if (targetObjectPos.x >= xMin && targetObjectPos.x <= xMax && targetObjectPos.y >= yMin && targetObjectPos.y <= yMax )
            {
                targetObjectPos = Camera.main.WorldToScreenPoint(TargetObject.transform.position) + new Vector3(Screen.width / 100f * TargetObjectOffset.x, Screen.height / 100f * TargetObjectOffset.y, 0f);
            }
            else{
                targetObjectPos = Camera.main.WorldToScreenPoint(SourcePosition.transform.position);
            }
        }
        else{
          targetObjectPos = Camera.main.WorldToScreenPoint(SourcePosition.transform.position);
        }

        GimmickTargetPos = targetObjectPos;

    }

    void UpdateRotation(){
        if(!Mathf.Approximately(RotationPulseDuration,0f)){
            currentTime += Time.deltaTime  / RotationPulseDuration;
            currentImageGimmickEuler = new Vector3(InitialGimmickRotation.x + AnimationScale * xRot.Evaluate(currentTime), InitialGimmickRotation.y + AnimationScale * yRot.Evaluate(currentTime), InitialGimmickRotation.z + AnimationScale * zRot.Evaluate(currentTime));
            GimmickImage.transform.localEulerAngles = currentImageGimmickEuler;
        }
    }

    void UpdatePosition()
    {
      
        transform.position = Vector3.Lerp(transform.position, GimmickTargetPos, MoveSpeed * Time.deltaTime);
    }


}
