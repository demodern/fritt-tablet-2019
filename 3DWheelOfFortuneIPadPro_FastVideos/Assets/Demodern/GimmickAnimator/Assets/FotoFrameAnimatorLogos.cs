﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FotoFrameAnimatorLogos : MonoBehaviour {


    public GameObject TargetObject;
    public Vector3 SourcePosition;
    public float FrameOffsetX = 0f;
    public GameObject FotoFrame;
    public float MoveSpeed = 1f;
    public Vector3 Initialposition;
    float currentTime;
 
    Vector3 GimmickTargetPos;
    Vector3 TargetScale;
    bool NeedsTransformationUpdate = false;
    Vector3 targetObjectPos;


    // Use this for initialization
    void Start () {
        Debug.Log("Start called;");
        currentTime = 0f;
        InitStartTransformations();
        TargetScale = Vector3.one;
    }
	
	// Update is called once per frame
	void Update () {   
        UpdatePosition();
    }

    public void InitStartTransformations()
    {
        transform.localPosition = Initialposition;
        NeedsTransformationUpdate = false;
    }

   public void SetToFacePos(Vector3 Target)
    {
        GimmickTargetPos = Target;
        NeedsTransformationUpdate = true;
    }
  

    void UpdatePosition()
    {
        if (NeedsTransformationUpdate)
        {
            transform.position = Vector3.Lerp(transform.position, GimmickTargetPos, MoveSpeed * Time.deltaTime);
           // transform.localScale = Vector3.Lerp(transform.localScale, TargetScale, MoveSpeed * Time.deltaTime);
        }
    }


}
