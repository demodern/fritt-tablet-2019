﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class FotoFrameAnimator : MonoBehaviour {


    public GameObject TargetObject;
    public Vector3 SourcePosition;
    public float FrameOffsetX = 0f;
    public float FrameOffsetYInFace = 0f;
    public GameObject FotoFrame;
    public float MoveSpeed = 1f;
    public FotoFrameAnimatorLogos[] Logos;
    //public Tracker visageTracker;
    public VideoPlayer vp;
    public Camera MainCamera;
    



    float currentTime;
 
    Vector3 GimmickTargetPos;
    Vector3 TargetScale;
 

    public  float yMin; //987.19
    public float yMax; // -73.2

    public bool faceWasFound = false;
    bool NeedsTransformationUpdate = false;

    Vector3 targetObjectPos;
    bool PhotoCountdownIsRunning = false;
    float countdownTime = 0f;
    public float MaxCountdownTime = 0f;
    public ScreenShotManager ssm;
    bool FramePositionIsSet = false;

    // Fuck off visage Tracking is called even if there is no trackied face
    int TrackedFaceCallCounter = 0;

    // Use this for initialization
    void Start () {
        Debug.Log("Start called;");
        currentTime = 0f;

       // SourcePosition.transform.SetParent(null);
        SourcePosition = new Vector3(Screen.width/2f, Screen.height/2f, 0f);
        targetObjectPos = SourcePosition;
        //gameObject.GetComponent<MeshRenderer>().enabled = false;

        Debug.Log("GimmickTargetPos: " + MainCamera.WorldToScreenPoint(TargetObject.transform.position));
        Vector3[] v = new Vector3[4];

            Debug.Log("World Corners");
            for (var i = 0; i < 4; i++)
            {
                Debug.Log("World Corner " + i + " : " + v[i]);
            }
        //xMin = v[0].x;
        //xMax = v[2].x;
        InitStartTransformations();
        TargetScale = Vector3.one;

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 testPos = MainCamera.WorldToScreenPoint(TargetObject.transform.position);
        Vector3 xxx = new Vector3(Screen.width / 2f, testPos.y, 0f) + new Vector3(FrameOffsetX, FrameOffsetYInFace, 0f);

        Debug.Log("xxx: " + xxx);
        if (!faceWasFound)
        {
           
        }
        GetTargetObjectScreenPos();

        UpdatePosition();


        if (PhotoCountdownIsRunning)
        {
            countdownTime += Time.deltaTime;
            if (countdownTime >= MaxCountdownTime)
            {
                ssm.Photo();
                PhotoCountdownIsRunning = false;
            }
        }
        CheckIfFaceIsTracked();
    }

    public void InitStartTransformations()
    {
        TrackedFaceCallCounter = 0;
        FramePositionIsSet = false;
        PhotoCountdownIsRunning = false;
        countdownTime = 0f;

        Vector3 Initialposition = new Vector3(0f, -556f, 0f);
        transform.localPosition = Initialposition;
        Vector3 InitialScale = new Vector3(1f, 1.35f, 1f);
        transform.localScale = InitialScale;
        NeedsTransformationUpdate = false;
        faceWasFound = false;
        InitLogos();
    }

    public void FaceIsTracked()
    {
        //Debug.Log("Face TrackedCalled");
        TrackedFaceCallCounter++;
        if(TrackedFaceCallCounter == 3)
        {
            faceWasFound = true;
        }
        
    }

    void CheckIfFaceIsTracked()
    {
        if (TargetObject.activeInHierarchy)
        {
            faceWasFound = true;
        }
    }

    void GetTargetObjectScreenPos(){

        
      
        if ( !FramePositionIsSet && faceWasFound) 
        {
            // targetObjectPos = Camera.main.WorldToScreenPoint(TargetObject.transform.position);


            //if (targetObjectPos.x >= xMin && targetObjectPos.x <= xMax )
            {

                Vector3 tmpTargetPos = MainCamera.WorldToScreenPoint(TargetObject.transform.position);
                targetObjectPos = new Vector3(Screen.width / 2f, tmpTargetPos.y, 0f) + new Vector3(FrameOffsetX, FrameOffsetYInFace, 0f);
                Debug.Log("targetObjectPos: " + targetObjectPos);

                if (targetObjectPos.y > yMax)
                {
                    targetObjectPos.y = yMax;
                }
                if (targetObjectPos.y < yMin)
                {
                    targetObjectPos.y = yMin;
                }


                //Debug.Log("DetTo Pos: " + targetObjectPos);
                NeedsTransformationUpdate = true;
                StartLogoPositioning(targetObjectPos);
                FramePositionIsSet = true;
                PlayCountDown();
                GimmickTargetPos = targetObjectPos; //targetObjectPos;
            } 
        }
    }

    public void PlayCountDown()
    {
        vp.Play();
        PhotoCountdownIsRunning = true;
    }

    public void InitLogos()
    {
        foreach (FotoFrameAnimatorLogos ffal in Logos)
        {
            ffal.InitStartTransformations();

        }
    }

    public void StartLogoPositioning(Vector3 MoveToPos)
    {
        foreach(FotoFrameAnimatorLogos ffal in Logos)
        {
            ffal.SetToFacePos(MoveToPos);
        }
    }
   


    void UpdatePosition()
    {
        if (NeedsTransformationUpdate)
        {
            transform.position = Vector3.Lerp(transform.position, GimmickTargetPos, MoveSpeed * Time.deltaTime);
            transform.localScale = Vector3.Lerp(transform.localScale, TargetScale, MoveSpeed * Time.deltaTime);
        }
    }


}
