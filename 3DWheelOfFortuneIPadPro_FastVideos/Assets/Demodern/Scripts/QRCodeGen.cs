﻿using System.Collections;
using System.Collections.Generic;
using AWSSDK;
using UnityEngine;
using UnityEngine.UI;

using ZXing;
using ZXing.QrCode;


public class QRCodeGen : MonoBehaviour {

    public Image DebugImage;
    public string QRContentString;
    public PrintGenerator pg;


    // Use this for initialization
    void Start () {
        //Attach to event
        Uploader.UploadStarted += GenerateQRCodeFromString;
    }
	
    public void GenerateQRCode(){
        Texture2D myQR = generateQR(QRContentString);
        DebugImage.overrideSprite = Sprite.Create(myQR, new Rect(0, 0, myQR.width, myQR.height), new Vector2(0.5f, 0.5f));
        DebugImage.preserveAspect = true;
    }
    public void GenerateQRCodeFromString(string QRContent)
    {
        string FrittURL = "https://gluecksrad.fritt.de/?image=" + QRContent;
        Debug.Log("Generate QR Code:"+ FrittURL);
        Texture2D myQR = generateQR(FrittURL);
        Sprite tmpSprite = Sprite.Create(myQR, new Rect(0, 0, myQR.width, myQR.height), new Vector2(0.5f, 0.5f));
        DebugImage.overrideSprite = tmpSprite;
        DebugImage.preserveAspect = true;
        StaticExample.QRCodeImage = myQR;
        StaticExample.QRCodeSprite = tmpSprite;
        pg.GeneratePrintScreenshot();
    }

    public Texture2D generateQR(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }


    private static Color32[] Encode(string textForEncoding,
  int width, int height)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }


  
}
