﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{

    public float animationLength = 1f;
    public AnimationCurve AnimCurve;
    public AnimationCurve AnimCurveRotation;

    private GameObject TargetObject;
    private float currentAnimationTime = 0f;
    //    private Transform FromTransform;
    bool needsUpdate = true;
    private GameObject FromGO;


    // Use this for initialization
    void Start()
    {
        FromGO = GameObject.CreatePrimitive(PrimitiveType.Cube);
        FromGO.GetComponent<MeshRenderer>().enabled = false;
        FromGO.GetComponent<Collider>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (needsUpdate && TargetObject != null)
        {
            currentAnimationTime += Time.deltaTime / animationLength;
            // Debug.Log("AnimCurve.Evaluate(currentAnimationTime): " + AnimCurve.Evaluate(0.5f));
            transform.position = Vector3.Lerp(FromGO.transform.position, TargetObject.transform.position, AnimCurve.Evaluate(currentAnimationTime));
            transform.rotation = Quaternion.Slerp(FromGO.transform.rotation, TargetObject.transform.rotation, AnimCurveRotation.Evaluate(currentAnimationTime));

            if (currentAnimationTime > 1f)
            {
                needsUpdate = false;
            }

        }
    }

    public void MoveToTarget(GameObject go)
    {

        //Debug.Log("MoveToTarget called");
        FromGO.transform.position = transform.position;
        //FromTransform = transform;
        TargetObject = go;
        currentAnimationTime = 0f;
        needsUpdate = true;
    }

    public void InstantSetToTarget(GameObject go)
    {
        //Debug.Log("InstantSetToTarget called");
        transform.position = go.transform.position;
        transform.rotation = go.transform.rotation;

    }
}
