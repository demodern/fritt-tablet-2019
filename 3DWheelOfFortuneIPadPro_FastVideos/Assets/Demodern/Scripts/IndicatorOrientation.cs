﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorOrientation : MonoBehaviour
{
    public GameObject PlanetCenter;
    public GameObject RotatingObject;
    public float  angleThreshold = 0f;
    public float timeThreshold = 0f;

    Quaternion prevQuat;
    Quaternion currQuat;

    float prevTime = 0f;
    float currTime = 0f;

    GameObject currentHitObject;
    // Use this for initialization
    void Start()
    {
        prevQuat = Quaternion.identity;
        currQuat = Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(PlanetCenter.transform);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.forward, 20f);

        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];
            Renderer rend = hit.transform.GetComponent<Renderer>();
            if (hit.transform.tag == "WinField")
            {
                if (currentHitObject != hit.transform.gameObject)
                {
                    if (currentHitObject != null)
                    {
                        currentHitObject.GetComponent<ColorController>().SetDarkMode();
                    }

                    currQuat = RotatingObject.transform.rotation;
                    currTime = Time.time;

                    currentHitObject = hit.transform.gameObject;
                    currentHitObject.GetComponent<ColorController>().SetBrightMode();

                    float deltaTimeWinfield = currTime - prevTime;
                    if (QuaternionDelta() > angleThreshold && deltaTimeWinfield > timeThreshold)
                    {
                    currentHitObject.GetComponent<EasyFXAudioPlayer>().PlayAudioClip();
                        prevTime = currTime;
                        prevQuat = currQuat;
                    }
                    //Debug.Log("New HitObject:" + currentHitObject.name);

                   
                    
                }
            }
        }
    }


    public GameObject GetCurrentHitObject()
    {
        return currentHitObject;
    }

    float QuaternionDelta()
    {

        float AngleDiff = Quaternion.Angle(prevQuat, currQuat);
       // Debug.Log("angle delta:" + AngleDiff);
       
        return AngleDiff;
    }

}
