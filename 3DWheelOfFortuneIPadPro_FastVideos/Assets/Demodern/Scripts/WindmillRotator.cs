﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindmillRotator : MonoBehaviour {
    public float SpeedX = 0f;
    public float SpeedY = 0f;
    public float SpeedZ = 0f;

    public bool RandomizeX = false;
    public bool RandomizeY = false;
    public bool RandomizeZ = false;

    public bool useInitialRotation = false;

    float initialX = 0f;
    float initialY = 0f;
    float initialZ = 0f;

    Vector3 InitialEulerAngles;

    float currentTime = 0f;
    // Use this for initialization
    void Start()
    {
        InitialEulerAngles = transform.localEulerAngles;
        if (RandomizeX) initialX = Random.Range(0.0f, 360.0f);
        if (RandomizeY) initialX = Random.Range(0.0f, 360.0f);
        if (RandomizeZ) initialX = Random.Range(0.0f, 360.0f);
        InitialEulerAngles = transform.localEulerAngles + new Vector3(initialX, initialY, initialZ);
    }

    // Update is called once per frame
    void Update()
    {
        currentTime = Time.time;
        if (!useInitialRotation)
        {
            transform.localEulerAngles = new Vector3(SpeedX * currentTime+ initialX, SpeedY * currentTime+ initialY, SpeedZ * currentTime+ initialZ);
        }
        else
        {
            transform.localEulerAngles = new Vector3(InitialEulerAngles.x + SpeedX * currentTime, InitialEulerAngles.y + SpeedY * currentTime, InitialEulerAngles.z + SpeedZ * currentTime);
        }
    }
}
