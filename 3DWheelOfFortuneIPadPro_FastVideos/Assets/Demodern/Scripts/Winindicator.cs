﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Winindicator : MonoBehaviour {

	public GameObject WinfieldContainer;
    public GameObject IndicatorRoot;
    public AnimationCurve AnimCurve;

    List<GameObject> Winfields;

    void Start () {
		Winfields = new List<GameObject>();
		CollectWinfields();
		InitWinFields();
	}
	
	// Update is called once per frame
	void Update () {
        DrawIndicatorDebugLine();

    }

	void InitWinFields(){
		foreach (GameObject go in Winfields){

         
                go.AddComponent<WinningField>();
                go.GetComponent<WinningField>().aCurve = AnimCurve;
                go.AddComponent<ColorController>();
                go.GetComponent<ColorController>().SetBrightMode();
                  
        }
	}

	void CollectWinfields(){
 	Transform[] t = WinfieldContainer.GetComponentsInChildren<Transform>();

        foreach (Transform tr in t){
            if (tr.gameObject.name.Contains("globetile"))
            {
                Winfields.Add(tr.gameObject);
            }
        }
	}

    private void DrawIndicatorDebugLine(){
        Debug.DrawLine(IndicatorRoot.transform.position, WinfieldContainer.transform.position);
    }

    public void SetAllFieldsToDark()
    {
        Debug.Log("SetAllFieldsToDark called");
        foreach (GameObject go in Winfields)
        {
            go.GetComponent<ColorController>().SetDarkMode();
        }
    }

    public void SetAllFieldsToLight()
    {
        foreach (GameObject go in Winfields)
        { 
            go.GetComponent<ColorController>().SetBrightMode();
        }
    }


}
