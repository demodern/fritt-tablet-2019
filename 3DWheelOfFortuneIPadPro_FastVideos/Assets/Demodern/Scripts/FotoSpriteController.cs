﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FotoSpriteController : MonoBehaviour {
    public GameObject FaceTarget;
    public Image LeftSprite;
    public Image RightSprite;
    public Image DebugImage;
    public GameObject TransformationContainer;
    public float yOffsetPercent =0;
    float yOffsetPixel = 0f;

	// Use this for initialization
	void Start () {
       

    }
	
	// Update is called once per frame
	void Update () {
        SetSpriteOrientation();
    }


    void SetSpriteOrientation(){

        yOffsetPixel = yOffsetPercent * (Screen.height/100f);


        Vector3 screenPos = Camera.main.WorldToScreenPoint(FaceTarget.transform.position);
        Vector3 OffsetSpritePos = new Vector3(0f, screenPos.y + yOffsetPixel, 0f);


        Vector3 Stuetzvector1 = new Vector3(1f, 0f, 0f);
        Vector3 Stuetzvector2 = FaceTarget.transform.position - OffsetSpritePos;


        Debug.DrawLine(FaceTarget.transform.position, new Vector3(1f, 0f, 0f));

        // Vector3 LeftImagePos = new Vector3(0f, screenPos.y, 0f);

        float Angle =Vector3.SignedAngle(Stuetzvector1, Stuetzvector2, new Vector3(0f, 0f, -1f));
        Debug.Log("Angle: " + Angle);
        DebugImage.transform.position = OffsetSpritePos;
        DebugImage.transform.localEulerAngles = new Vector3(0f, 0f, Angle);
    }

}
