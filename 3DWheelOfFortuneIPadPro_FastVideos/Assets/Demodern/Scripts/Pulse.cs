﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

    public AnimationCurve ScaleAnimCurve;
    public float AnimationDuration;
    Vector3 initialScale;
    float currentTime = 0f;

    // Use this for initialization
    void Start () {
        initialScale = transform.localScale;
    }
    
    // Update is called once per frame
    void Update () {

        currentTime += Time.deltaTime / AnimationDuration;
        if(currentTime > AnimationDuration)
        {
            currentTime = currentTime - AnimationDuration;
        }

        float currentScaleVectorComponent =ScaleAnimCurve.Evaluate(currentTime);
        transform.localScale = new Vector3(initialScale.x + currentScaleVectorComponent, initialScale.y + currentScaleVectorComponent, initialScale.z + currentScaleVectorComponent);
    }
}
