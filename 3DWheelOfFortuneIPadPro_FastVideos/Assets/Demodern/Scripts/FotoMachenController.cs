﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FotoMachenController : MonoBehaviour {


  
    public EasyFXMultiController FotoMachenControllerButton;
    public EasyFXMultiController HomeController;

    public Toggle SelectHomeFoto;

    public  EasyFXMultiController currentController;

    public bool BackToHomeSelected ;

    // Use this for initialization
    void Start()
    {
        BackToHomeSelected = false;
        currentController.EndTriggered += PlayNextScene;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayNextScene() // back to home
    {

        if (BackToHomeSelected)
        {

        }
        else // start visage
        {

        }
    }

    public void StartFotoMachen()
    {
        Init();
        currentController.PlayStart();
        HomeController.PlayStart();
        SelectHomeFoto.isOn = false;
        

        //HomeController.PlayEnd(); 
    }

    public void Init()
    {
     
        BackToHomeSelected = false;
    }

    public void ShowButtonHome()
    {
        FotoMachenControllerButton.PlayEnd();
        HomeController.PlayStart();
      
        BackToHomeSelected = true;
    }

    public void ShowButtonFotoMachen()
    {
        FotoMachenControllerButton.PlayStart();
        HomeController.PlayEnd();
    
        BackToHomeSelected = false;
    }

    public void FotoMachen()
    {
        currentController.NextMenuIndex = 0;
        FotoMachenControllerButton.PlayEnd();
        currentController.PlayEnd();
        BackToHomeSelected = false;
    }

    public void BackToHome()
    {
       // currentController.NextMenuIndex = 1;
        currentController.PlayEnd();
        HomeController.PlayEnd();
        BackToHomeSelected = true;
    }

    public void toggleHomeFoto()
    {
        bool state = SelectHomeFoto.isOn;
        Debug.Log("toggleHomeFoto called: " + state);
        if (state)
        {
            HomeController.PlayEnd();
            FotoMachenControllerButton.PlayStart();
            BackToHomeSelected = false;

           
        }
        else
        {
            HomeController.PlayStart();
            FotoMachenControllerButton.PlayEnd();
            BackToHomeSelected = true;
        }
    }
}
