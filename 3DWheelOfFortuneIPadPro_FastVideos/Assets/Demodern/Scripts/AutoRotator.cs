﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotator : MonoBehaviour {

    public float SpeedX = 0f;
    public float SpeedY = 0f;
    public float SpeedZ = 0f;
    public bool useInitialRotation = false;
    Vector3 InitialEulerAngles;

    float currentTime = 0f;
    // Use this for initialization
    void Start () {
        InitialEulerAngles = transform.localEulerAngles;

    }
	
	// Update is called once per frame
	void Update () {
        currentTime = Time.time;
        if (!useInitialRotation)
        {
        transform.localEulerAngles = new Vector3( SpeedX * currentTime, SpeedY * currentTime, SpeedZ * currentTime);
        }
        else
        {
            transform.localEulerAngles = new Vector3(InitialEulerAngles.x + SpeedX * currentTime, InitialEulerAngles.y + SpeedY * currentTime, InitialEulerAngles.z + SpeedZ * currentTime);

        }
    }
}
