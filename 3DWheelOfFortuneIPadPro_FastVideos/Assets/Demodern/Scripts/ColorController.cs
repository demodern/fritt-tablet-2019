﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorController : MonoBehaviour {

	private Material mat;
	public float currentAlpha = 0f;
	MeshRenderer mr;
	Material[] materials;
	public float fadeSpeed = 5f;
	public float targetAlpha = 1f;

    bool needsUpdate = true;
    Vector4 targetBrightnessColor;
    ColorController[] childColorController;


    void Start () {
		mr = gameObject.GetComponent<MeshRenderer> ();
		materials = mr.materials;
        mat = mr.material;
        targetBrightnessColor = Vector4.one;
        CollectColorControllerChildren();

    }

	void Update () {
        if (needsUpdate) {

            Vector4  newColor = mat.color;
            newColor = Vector4.Lerp(newColor, targetBrightnessColor, fadeSpeed * Time.deltaTime);
            mat.color = newColor;
            if ( Vector4.Distance(newColor, targetBrightnessColor) < 0.01)
            {
                mat.color = targetBrightnessColor;
                needsUpdate = false;
            }
         }
    }

    // manage child ColorController

    void CollectColorControllerChildren()
    {
        if (GetComponent<WinFieldIdentifier>() != null)
        {

        childColorController = GetComponent<WinFieldIdentifier>().childColorController;
        }
    }


    public void SetBrightMode()
    {
        SetToTargetBrightness(1f);
        SetChildrenToBrightness(1f);
    }

    public void SetDarkMode()
    {
        SetToTargetBrightness(0.6f);
        SetChildrenToBrightness(0.6f);
    }

    void SetChildrenToBrightness(float brightness)
    {
        //Debug.Log("finding children in: " + gameObject.name);
        // CollectColorControllerChildren();
        if (childColorController != null) {
            foreach (ColorController cc in childColorController)
            {
                cc.SetToTargetBrightness(brightness);
            }
        }
    }

    public void SetToTargetBrightness(float brightness){

        needsUpdate = true;
        targetBrightnessColor = new Vector4(brightness, brightness, brightness, 1f); 
	}

}
