﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using System;
using UnityEngine.UI;


//This enables the WebCam Texture to be shown on the Camera
public class webcamTexture : MonoBehaviour {

    [Header("References")]
    public GameObject videoScreen;

    [Header("Settings")]
    [RangeEx(30, 90, 30)]
    public int FPS = 60;
    public bool useScreenDimensions = true;

    [HideInInspector]
    public int Camera;
    [HideInInspector]
    public int selectedResolution = 0;
    [HideInInspector]
    public int width;
    [HideInInspector]
    public int height;

    private WebCamTexture backCamera;


    void Start () {
   
        if (useScreenDimensions)
        {
            width = Screen.width;
            height = Screen.height;
        }
        Debug.Log("Webcam Resolution was set to: " + width + " x " + height);

        //Enable Webcam with Screen Dimensions
        backCamera = new WebCamTexture(WebCamTexture.devices[Camera].name, width, height, FPS);

        //Show Webcam Texture on a Plane
        videoScreen.GetComponent<Renderer>().material.mainTexture = backCamera;
        backCamera.Play();
    }

 
}

