﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutorotateZeppelinHinten : MonoBehaviour {
	public float speed = 1f;
	// Use this for initialization
	void Start () {
		
	}
	
	void Update ()
	{
		transform.Rotate(Vector3.forward, speed * Time.deltaTime);
	}
}
