﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class rotObj : MonoBehaviour 
{
	public float rotSpeed = 20;
	public float FrictionValue = 2;
	public float RotationThreshold = 0.1f;
	public Animator Wining;
//	float prevRotX;
//	float prevRotY;

	float rotX = 0.1f;
	float rotY = 0.05f;

	public bool isWon = false;

	public Slider FrictionSlider;
	public Text FrictionText;

	public Slider SpeedSlider;
	public Text SpeedText;

//	bool WheelIsSpinning;

	void Start(){
//		WheelIsSpinning = false;
		isWon = true;
	}

	void OnMouseDrag()
	{
		rotX = Input.GetAxis("Mouse X")*rotSpeed*Mathf.Deg2Rad;
		rotY = Input.GetAxis("Mouse Y")*rotSpeed*Mathf.Deg2Rad;

	//	prevRotX = rotX;
	//	prevRotY = rotY;
	}

	void OnMouseDown(){
		Debug.Log ("Drag started");
	}

	void OnMouseUp()
	{
		// If your mouse hovers over the GameObject with the script attached, output this message
		Debug.Log("Drag ended!");
		isWon = false;
	}

	public void SetFriction(){
		FrictionValue = FrictionSlider.value;
		FrictionText.text = FrictionValue.ToString ();	
	}

	public void SetSpeed(){
		rotSpeed = SpeedSlider.value;
		SpeedText.text = rotSpeed.ToString ();	
	}


	void Update(){
		//Debug.Log (rotX + " " + rotY);
		rotX = rotX - rotX / FrictionValue;
		rotY = rotY - rotY / FrictionValue;

        transform.Rotate(Vector3.up, -rotX);
        transform.Rotate(Vector3.right, rotY);	

		if (Mathf.Abs (rotX + rotY) <= RotationThreshold  ) {
			rotX = 0;
			rotY = 0;
			if(isWon == false){
			ShowWin ();
			}
		}
	}


	public void ShowWin(){
		Debug.Log ("You Win");	
		Wining.SetTrigger ("Win");
		isWon = true;
	}

	public void TriggerWin(){
		Wining.SetTrigger ("Win");
	}



}