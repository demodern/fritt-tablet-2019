﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public class AssetProvider : MonoBehaviour {

    public int WinfieldID;
    public VideoClip[] Clips;
    public Texture2D[] PolaroidImages;
    public float[] MovieDurationsTillBlend;
    public VideoPlayerDelay[] VideoplayerCharacter;
    public float BlendTime = 1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
