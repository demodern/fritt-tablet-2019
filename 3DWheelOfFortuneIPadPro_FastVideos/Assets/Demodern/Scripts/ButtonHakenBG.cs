﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHakenBG : MonoBehaviour {

    public GameObject Haken;
    public EasyFXMultiController FotoMachenController;
    public EasyFXMultiController HomeController;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowFotoMachen()
    {
        FotoMachenController.PlayStart();
        HomeController.PlayEnd();
        Haken.SetActive(true);

    }
}
