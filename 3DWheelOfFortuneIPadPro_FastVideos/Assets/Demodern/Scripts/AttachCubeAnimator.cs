﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class AttachCubeAnimator : MonoBehaviour
{

    public AnimationCurve aCurve;
    public float AnimationSpeed = 1f;
    public float CureScale = 0.1f;
    public CameraMover cm;
    public VideoPlayer vp;


    bool needsUpdate = false;
    //Vector3 initialAngle;
    private Transform FromTransform;
    Vector3 TargetAngle;



    float currentTime = 0f;
    // Use this for initialization
    void Start()
    {
        //gameobject
        FromTransform = transform;
        TargetAngle = new Vector3(0f, 180f, 0f);
        //initialAngle = transform.localEulerAngles;

    }

    // Update is called once per frame
    void Update()
    {
        if (needsUpdate)
        {
            currentTime += Time.deltaTime / AnimationSpeed;
            transform.localEulerAngles = Vector3.Slerp(FromTransform.localEulerAngles, TargetAngle, aCurve.Evaluate(currentTime));

            if (currentTime > 1f)
            {
                needsUpdate = false;
                vp.Play();
            }
        }
    }


    public void StartAnimation()
    {

        currentTime = -cm.animationLength;
        needsUpdate = true;
    }

    public void SetWinClip(VideoClip vc)
    {
        vp.clip = vc;
        vp.Prepare();
    }
}
