﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerMain : MonoBehaviour {

    public AudioControllerSimple AudioControllerBG;
    public AudioControllerSimple AudioControllerSong;
    public AudioControllerSimple AudioControllerZoom;

    public EasyFXMultiController GameStartMenu;
    
    // Use this for initialization
    void Start () {

        SwitchToIdle();
        GameStartMenu.StartTriggered = SwitchToIdle;
    }

    // Update is called once per frame
    void Update () {

       

		
	}


    public void SwitchToIdle()
    {
        AudioControllerSong.FadeOut();
        AudioControllerBG.FadeIn();
    
    }


    public void SwitchToSong()
    {
        AudioControllerZoom.FadeIn();
        AudioControllerSong.aSource.Stop();
        AudioControllerSong.aSource.Play();
        AudioControllerSong.FadeIn();
        AudioControllerBG.FadeOut();
        AudioControllerZoom.aSource.Play();
        Debug.Log("Zoom wird abgespielt");


    }



}
