﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeController : MonoBehaviour {

	private Material mat;
	public float currentAlpha = 0f;
	MeshRenderer mr;
	Material[] materials;
	public float fadeSpeed = 1f;

	public float AlphaTapStep = 0.3f;
	public float targetAlpha = 1f;




	void Start () {
		mr = gameObject.GetComponent<MeshRenderer> ();
		materials = mr.materials;
	}

	void Update () {
		SetToTargetAlpha ();
	}

	void SetToTargetAlpha(){
		for (int i = 0; i < materials.Length; i++) {
			mat = materials[i];
			Color newColor = mat.color;
			currentAlpha = newColor.a;
			newColor.a = Mathf.Lerp (newColor.a, targetAlpha,fadeSpeed*Time.deltaTime);
			//mat.color = newColor;
			mr.materials[i].color = newColor;
		}
	}

	public float IncreaseAlphaByTap(float increase){
		fadeSpeed = 1f;
		currentAlpha = Mathf.Clamp01 (currentAlpha + increase);

		for (int i = 0; i < materials.Length; i++) {
			mat = materials[i];
			Color newColor = mat.color;
			newColor.a = currentAlpha;
			mr.materials[i].color = newColor;
		}

		return currentAlpha;
	}

	public void SetInstantToAlpha(float alpha){
		mr = gameObject.GetComponent<MeshRenderer> ();
		materials = mr.materials;
		for (int i = 0; i < materials.Length; i++) {
			mat = materials[i];
			Color newColor = mat.color;
			currentAlpha = alpha;
			targetAlpha = alpha;
			newColor.a = alpha;
			//mat.color = newColor;
			mr.materials[i].color = newColor;
		}
	}


}
