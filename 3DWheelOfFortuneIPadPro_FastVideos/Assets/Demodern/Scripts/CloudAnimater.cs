﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloudAnimater : MonoBehaviour {
    public float Animationspeed = 0.01f;

    Material CloudMaterial;

    float currentTime = 0;
	// Use this for initialization
	void Start () {
        CloudMaterial = GetComponent<Image>().material;

    }
	
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime / Animationspeed;
        CloudMaterial.SetTextureOffset("_MainTex", new Vector2(currentTime, 0));

    }
}
