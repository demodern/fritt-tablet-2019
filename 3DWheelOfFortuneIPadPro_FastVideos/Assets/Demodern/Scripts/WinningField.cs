﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningField : MonoBehaviour {

    public AnimationCurve aCurve;
    public float AnimationSpeed = 1f;
    public float CureScale = 0.1f;

    Vector3 initialScale;
    bool needsUpdate = false;


    float currentTime = 0f;
	// Use this for initialization
	void Start () {
        //gameobject
        gameObject.AddComponent<MeshCollider>();
        initialScale = transform.localScale;
    }
	
	// Update is called once per frame
    void Update () {

        if (needsUpdate)
        {
            currentTime += Time.deltaTime / AnimationSpeed;
            transform.localScale = new Vector3(initialScale.x + aCurve.Evaluate(currentTime) * CureScale, initialScale.y + aCurve.Evaluate(currentTime) * CureScale, initialScale.z + aCurve.Evaluate(currentTime) * CureScale);

            if (currentTime >= 1f)
            {
                transform.localScale = new Vector3(initialScale.x + aCurve.Evaluate(1f) * CureScale, initialScale.y + aCurve.Evaluate(1f) * CureScale, initialScale.z + aCurve.Evaluate(1f) * CureScale);
                needsUpdate = false;
            }
        }
    }
     
    public void StartAnimation(){
        currentTime = 0f;
        needsUpdate = true;
    }

    public void LightenUp()
    {
        GetComponent<Renderer>().material.color = new Vector4(1f, 1f, 1f, 1f);

    }

    public void Darken()
    {
        GetComponent<Renderer>().material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);
    }
}
