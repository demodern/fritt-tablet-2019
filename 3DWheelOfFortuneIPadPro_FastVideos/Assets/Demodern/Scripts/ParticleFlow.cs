﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleFlow : MonoBehaviour
{
	ParticleSystem m_System;
	ParticleSystem.Particle[] m_Particles;
	public float forceFactor = 10f;

	public GameObject TargetObject;

	private void LateUpdate()
	{
		InitializeIfNeeded();

		// GetParticles is allocation free because we reuse the m_Particles buffer between updates
		int numParticlesAlive = m_System.GetParticles(m_Particles);

		// Change only the particles that are alive
		for (int i = 0; i < numParticlesAlive; i++)
		{
			float distance = Vector3.Distance(TargetObject.transform.position, m_Particles[i].position);
			if (distance > 0.1f)
			{
				Vector3 velocity = (TargetObject.transform.position - m_Particles [i].position) *forceFactor;
				m_Particles [i].velocity = m_Particles [i].velocity = velocity;
			}
		}

		// Apply the particle changes to the particle system
		m_System.SetParticles(m_Particles, numParticlesAlive);
	}

	void InitializeIfNeeded()
	{
		if (m_System == null)
			m_System = GetComponent<ParticleSystem>();

		if (m_Particles == null || m_Particles.Length < m_System.main.maxParticles)
			m_Particles = new ParticleSystem.Particle[m_System.main.maxParticles];
	}
}