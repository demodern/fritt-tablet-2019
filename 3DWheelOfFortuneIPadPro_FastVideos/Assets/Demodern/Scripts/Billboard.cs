﻿using UnityEngine;

public class Billboard : MonoBehaviour
{
    public GameObject Planet;
    void Update()
    {
       
    }


    public void CalcTargetCamPos(){
        Vector3 upVector = Planet.transform.position - transform.position;
        Debug.DrawLine(transform.position, transform.position + upVector * 5f);
        transform.LookAt(Camera.main.transform.position, new Vector3(0f, 0f, 1f));
        transform.localEulerAngles = new Vector3(0f, 0f, -transform.localEulerAngles.z);
    }
   
}