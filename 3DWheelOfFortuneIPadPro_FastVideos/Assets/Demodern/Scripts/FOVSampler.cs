﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOVSampler : MonoBehaviour {

	public Camera SampleCam;
    Camera myCam;
	// Use this for initialization
	void Start () {
        myCam = GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        myCam.fieldOfView = SampleCam.fieldOfView;
	}
}
