﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Rendering;
//using UnityEngine.PostProcessing;
using System;

public class MaskController : MonoBehaviour {
    
    public bool ShowMask = false;
    public VideoClip spruchVideo;
    public GameObject[] MaskObjects;
    //public PostProcessingProfile ppp;

    //PostProcessingBehaviour VisageCamera;
    //PostProcessingBehaviour ScreenshotWebcam;
    VideoPlayer TextRevealVideo;

    private void Update()
    {
        //gameObject.SetActive(true);
    }
    void Start()
    {
        //Find References in Scene
        //VisageCamera = GameObject.Find("Video Camera").GetComponent<PostProcessingBehaviour>();
  
       //ScreenshotWebcam = GameObject.Find("Webcam").GetComponent<PostProcessingBehaviour>();
   
        HideOrShowObjects();

        if (ShowMask)
            ActivateEffects();
    }

    //Called from the Outside
    public void SetActive(bool Active)
    {
        ShowMask = Active;
        if (ShowMask)
            ActivateEffects();
        
        HideOrShowObjects();
    }

    void ActivateEffects(){
        //StartVideo();
        EnablePPP();  
    }

    void EnablePPP()
    {
       // VisageCamera.profile = ppp;
       // ScreenshotWebcam.profile = ppp;
    }

    void StartVideo()
    {
        TextRevealVideo.clip = spruchVideo;
    }

    void HideOrShowObjects(){
        foreach(GameObject go in MaskObjects)
        {
            go.SetActive(ShowMask);
        }
    }
}
