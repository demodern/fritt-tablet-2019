﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerSimple : MonoBehaviour {

    public float FadeTime;
    public float fadeDelayStart = 0f;
    public float fadeDelayEnd = 0f;
  
   public AudioSource aSource;
    float currentTime = 0f;

    float TargetVolume = 0;

    bool NeedsUpdate = false;
    float startVolume = 0F;
    float CurrentVolume = 0f;
    public float MyVolume = 0f;

    public float VolumeMax = 1f;
    public float VolumeMin = 0f;
    // Use this for initialization
    void Start () {

        aSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update () {

        if (NeedsUpdate && aSource !=  null)
        {
            currentTime += Time.deltaTime/ FadeTime;
            if (currentTime >1f)
            {
                currentTime = 1f;
                NeedsUpdate = false;
            }
           
            CurrentVolume =  Mathf.Lerp(startVolume, TargetVolume, currentTime);
            MyVolume = CurrentVolume;
            aSource.volume = CurrentVolume;
        }

		
	}

    public void FadeIn()
    {
        FadeTo(VolumeMax, fadeDelayStart);
        //Debug.Log("Fade In Called");
    }

    public void FadeOut()
    {
        FadeTo(VolumeMin, fadeDelayEnd);
        //Debug.Log("Fade Out Called");
    }

    void FadeTo(float targetVolume, float Offset)
    {
       // Debug.Log("FadeTo:" + targetVolume);
        TargetVolume = targetVolume;
        currentTime = -Offset;
        if (aSource == null)
        {
            aSource = GetComponent<AudioSource>();
            aSource.loop = true;
            aSource.Play();
            
        }
        startVolume = aSource.volume;
        NeedsUpdate = true;

    }


}
