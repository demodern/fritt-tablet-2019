﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This Interface is used for turning Masks (MaskController) off and on */
public class MaskInterface : MonoBehaviour {

    public MaskController[] Masks;
    private string[] MaskNames;

    //HandController[] Hands;

    void Awake () {
       /* Masks = GetComponentsInChildren<MaskController>();
        //Hands = GetComponentsInChildren<HandController>();
        MaskNames = new string[Masks.Length];
        InitMaskNames();*/
	}

    void InitMaskNames(){
        for (int i = 0; i < Masks.Length; i++){
            MaskNames[i] = Masks[i].gameObject.name;
            Debug.Log("Mask " + i + ": " + MaskNames[i]);
        }
    }

    public string[] getMaskName(){
        return MaskNames;
    }

    public void SetMaskActiveByName(string Name){

        foreach(MaskController Mask in Masks)
        {
                Mask.SetActive(false);
        }

        int MaskSelected = (int)Random.Range(0f, 6f);
        Masks[MaskSelected].SetActive(true);
    }

    public void SetMaskActiveByID(int ID)
    {
        foreach (MaskController Mask in Masks)
        {
            Mask.SetActive(false);
        }
        
        Masks[ID].SetActive(true);
    }

}
