﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisageManager : MonoBehaviour {
    public GameObject VisageSceneManager;
    public EasyFXMultiController FaceFilterStart;
    public EasyFXMultiController FaceFilterEnd;
    public EasyFXMultiController BackToHome;
    public EasyFXMultiController ShowPhoto;
    public EasyFXMultiController StartMenu;
    public EasyFXMultiController HomeBackground;
    public GameObject[] PlanetObjects;
    public Image PhotoPreview;
    public FotoMachenController fmc;
    public EasyFXMultiController fotoDruckenController;
    public GameObject BGBlende;
    public GameObject FotoMachenGO;
    public RawImage QRCodeImage;
    public GameObject Birds;

    public GameObject FotoMachenCanvas;

    string VisageSceneName = "VisageSceneManager";

    // Use this for initialization
    void Start () {
        VisageSceneManager = GameObject.Find(VisageSceneName);
        FaceFilterStart.MenuEndFinished += ShowVisage;
        FaceFilterEnd.EndTriggered += HideVisage;
        StartMenu.StartTriggered += PlayHomeBGAnimationEnd;
        FaceFilterStart.StartTriggered += PlayHomeBGAnimationStart;
    }

    public void SetQRImage()
    {
        QRCodeImage.texture = StaticExample.QRCodeImage;
    }

    void PlayHomeBGAnimationEnd()
    {
        Debug.Log("PlayHomeBGAnimationEnd called");
        BGBlende.SetActive(true);
        HomeBackground.PlayEnd();
        FotoMachenGO.SetActive(false);

        Birds.SetActive(true);

        // hier vögel deaktivieren
    }

    void PlayHomeBGAnimationStart()
    {
        Debug.Log("PlayHomeBGAnimationStart called");
        HomeBackground.PlayStart();
        Birds.SetActive(false);
        // hier vögel aktivieren
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void BackToHomeFromPrint()
    {
        fotoDruckenController.NextMenuIndex = 0;
        if (VisageSceneManager == null)
        {
            VisageSceneManager = GameObject.Find(VisageSceneName);
        }
        if (VisageSceneManager != null)
        {
            VisageSceneManager.GetComponent<VisageSceneManager>().DeActivateVisage();
            }
        fotoDruckenController.PlayEnd();
        BGBlende.SetActive(true);
        FotoMachenCanvas.SetActive(true);
    }


    public void ShowVisage()
    {
        BGBlende.SetActive(false);
        fotoDruckenController.NextMenuIndex = 0;
        if (fmc.BackToHomeSelected)
        {
            Debug.Log("ShowVisage end triggered");
            ActivatePlanet();
            StartMenu.PlayStart();
           
            return;
        }
        if (VisageSceneManager == null) {
            VisageSceneManager = GameObject.Find(VisageSceneName);
        }

        if (VisageSceneManager != null)
        {
            VisageSceneManager.GetComponent<VisageSceneManager>().ActivateVisage();
            VisageSceneManager.GetComponent<VisageSceneManager>().StartPhotoCountdown();
            VisageSceneManager.GetComponent<VisageSceneManager>().VM = GetComponent<VisageManager>();
        }
        DeActivatePlanet();
     }


 

    public void HideVisage()
    {
        if (VisageSceneManager == null)
        {
            VisageSceneManager = GameObject.Find(VisageSceneName);
        }
        if (VisageSceneManager != null)
        {
            VisageSceneManager.GetComponent<VisageSceneManager>().DeActivateVisage();
            VisageSceneManager.GetComponent<VisageSceneManager>().VM = GetComponent<VisageManager>();

        }
        ActivatePlanet();
    }

    public void ActivatePlanet()
    {
        SetObjectState(true);
    }

    public void DeActivatePlanet()
    {
        SetObjectState(false);
    }


    void SetObjectState(bool State)
    {
        foreach (GameObject go in PlanetObjects)
        {
            go.SetActive(State);
        }
    }


    public void PhotoIsDone()
    {
        HideVisage();
        PhotoPreview.sprite = Sprite.Create(StaticExample.UploadPhoto, new Rect(0, 0, StaticExample.UploadPhoto.width, StaticExample.UploadPhoto.height), new Vector2());
        ShowPhoto.PlayStart();
        FotoMachenCanvas.SetActive(false);

    }

    public void UploadAndSavePhoto()
    {
        if (VisageSceneManager == null)
        {
            VisageSceneManager = GameObject.Find(VisageSceneName);
        }
        if (VisageSceneManager != null)
        {
            VisageSceneManager.GetComponent<VisageSceneManager>().DeActivateVisage();
            VisageSceneManager.GetComponent<VisageSceneManager>().UploadAndSavePhoto();

        }
        BGBlende.SetActive(true);
    }
}
