﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotManager : MonoBehaviour {
    public string PathNameWithoutExtension = "";
    public int supersize = 1;
    public int currentIndex = 0;
    public Camera virtuCamera;
    public int width = 512;
    public int height = 512;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            currentIndex++;
            MakeSquarePngFromOurVirtualThingy();
            //TakeScreenshot();
        }


    }

    void TakeScreenshot()
    {
        string PathName = PathNameWithoutExtension + currentIndex.ToString()+".png";
        ScreenCapture.CaptureScreenshot(PathName, supersize);
    }

    public void MakeSquarePngFromOurVirtualThingy()
    {
        // capture the virtuCam and save it as a square PNG.

        int sqr = 512;

        //virtuCamera.camera.aspect = 1.0f;
        // recall that the height is now the "actual" size from now on

        RenderTexture tempRT = new RenderTexture(width, height, 24);
        // the 24 can be 0,16,24, formats like
        // RenderTextureFormat.Default, ARGB32 etc.

        virtuCamera.targetTexture = tempRT;
        virtuCamera.Render();

        RenderTexture.active = tempRT;
        Texture2D virtualPhoto =
            new Texture2D(width, height, TextureFormat.RGB24, false);
        // false, meaning no need for mipmaps
        virtualPhoto.ReadPixels(new Rect(0, 0, width, height), 0, 0);

        RenderTexture.active = null; //can help avoid errors 
        virtuCamera.targetTexture = null;
        // consider ... Destroy(tempRT);

        byte[] bytes;
        bytes = virtualPhoto.EncodeToPNG();
        string PathName = PathNameWithoutExtension + currentIndex.ToString() + ".png";

        System.IO.File.WriteAllBytes(PathName, bytes);
        // virtualCam.SetActive(false); ... no great need for this.
        
    }
}
