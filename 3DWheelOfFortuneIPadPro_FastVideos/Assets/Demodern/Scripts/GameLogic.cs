﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System;

public class GameLogic : MonoBehaviour {

    public CameraMover camMove;
    public GameObject CamStartPos;
    public GameObject CamWinPos;
    public IndicatorOrientation io;
    public GameObject Planet;
    public PlanetInteraction pInteraction;
    public Billboard TargetCamPosCalculator;
    public Winindicator WinIndi;
    public GameObject WinFieldAttachCube;
    public VideoClip[] WinClips;
    public VideoPlayerDelay VPD;
    public EasyFXMultiController LastController;
    public EasyFXMultiController FirstController;
    public EasyFXMultiController MultiControllerJetztDrehen;
    public Image PolaroidImage;
    public AudioControllerMain acm;




    // SetAssets in this classes




    // Debug Stuff
    public GameObject DebugSphere;


    GameObject CurrentWinfield;

    // Use this for initialization
    void Start () {
        Input.multiTouchEnabled = false;
        pInteraction.wheelStopped = SpinnIsFinished;
        LastController.StartTriggered += ResetCameraToStartPos;
        FirstController.StartTriggered  += EnablePlanetInteraction;
        SetPlanetInteractionState(false);
        WinIndi.SetAllFieldsToLight();
        io.gameObject.SetActive(false);
        InitVideoPlayers();
    }

    private void InitVideoPlayers()
    {
        AssetProvider ap = GetComponent<AssetProvider>();

       for(int i = 0; i < ap.VideoplayerCharacter.Length; i++)
        {
            ap.VideoplayerCharacter[i].SetVideoForPlayer( ap.Clips[i]);
            ap.VideoplayerCharacter[i].TimeOffsetForMulticontrollerPlay = ap.VideoplayerCharacter[i].TimeOffsetForVideoPlay + ap.MovieDurationsTillBlend[i];
            ap.VideoplayerCharacter[i].TimeDisableVideoImage = ap.VideoplayerCharacter[i].TimeOffsetForVideoPlay + ap.MovieDurationsTillBlend[i] + ap.BlendTime;
        }
    }

    // Update is called once per frame
    void Update () {
        //CalcCamWinPos();
    }


    public void StartGame(){
        camMove.MoveToTarget(CamStartPos);
    }

    public void SetPlanetInteractionState(bool isInteractable)
    {
        pInteraction.UserTouchIsActive = isInteractable;
        if (isInteractable)
        {
            pInteraction.SetIdleInteraction();
            pInteraction.UseAutorotate = false;
          

        }
        else
        {
            pInteraction.SetIdleInteraction();
       
        }
        
    }

    public void SpinnIsFinished()
    {
        // the next two function should become one
        /* CalcCamWinPos();
         TargetCamPosCalculator.CalcTargetCamPos();

         camMove.MoveToTarget(CamWinPos);

         int winClipIndex = Random.Range(0, WinClips.Length - 1);
         Debug.Log("winClipIndex: " + winClipIndex);

         WinFieldAttachCube.GetComponent<AttachCubeAnimator>().SetWinClip(WinClips[winClipIndex]);
         AttachWinFieldAndSpin();*/

      
       // if (io.GetCurrentHitObject() != null)
        {
            CurrentWinfield = io.GetCurrentHitObject();
            int WinfieldID = CurrentWinfield.GetComponent<WinFieldIdentifier>().WinFieldId;
           
            //Debug.Log("WinfieldID: " + WinfieldID);
            SetAssestsForWinfield(WinfieldID);
            GameObject Child = CurrentWinfield.transform.GetChild(0).gameObject;
            CamWinPos = Child.GetComponent<CameraTargetPositionController>().CamTarget;
            //VPD.StartVideoSequence();
            AssetProvider ap = GetComponent<AssetProvider>();
            Debug.Log("WinfieldID: " + WinfieldID);
            ap.VideoplayerCharacter[WinfieldID].StartVideoSequence();
        }
        camMove.MoveToTarget(CamWinPos);


       

        DisablePlanetInteraction();
        GetComponent<EasyFXAudioPlayer>().PlayAudioClip();
        acm.SwitchToSong();


    }

    void ResetCameraToStartPos()
    {
        Debug.Log("Reset Camera to Start Pos");
        camMove.InstantSetToTarget(CamStartPos);
        //camMove.MoveToTarget(CamStartPos);
        pInteraction.ResetInteractionState();
    }

    public void DisablePlanetInteraction()
    {
        pInteraction.enabled = false;
      
    }


    public void HideJetztDrehen()
    {
        MultiControllerJetztDrehen.PlayEnd();
    }


    public void EnablePlanetInteraction()
    {
        pInteraction.enabled = true;
      
        //camMove.MoveToTarget(CamStartPos);
        camMove.InstantSetToTarget(CamStartPos);
        pInteraction.ResetInteractionState();
        pInteraction.UseAutorotate = true;
        WinIndi.SetAllFieldsToLight();
        io.gameObject.SetActive(false);

    }

    public void ActivateWinfieldCollision()
    {
        io.gameObject.SetActive(true);
        WinIndi.SetAllFieldsToDark();
    }


    void CalcCamWinPos(){

        if (io.GetCurrentHitObject() != null)
        {
            CurrentWinfield = io.GetCurrentHitObject();
           
            DebugSphere.transform.rotation = CurrentWinfield.transform.rotation;
            DebugSphere.transform.position = CurrentWinfield.GetComponent<Collider>().bounds.center;
            DebugSphere.transform.LookAt(Planet.transform);
        }
    }

    void AttachWinFieldAndSpin(){
        GameObject winfield = io.GetCurrentHitObject();
        Vector3 globalWinfieldScale = winfield.transform.lossyScale;
        winfield.transform.SetParent(WinFieldAttachCube.transform);
        winfield.transform.localScale = globalWinfieldScale;  
        WinFieldAttachCube.GetComponent<AttachCubeAnimator>().StartAnimation();
    }


    void SetAssestsForWinfield(int winfieldID)
    {
        //Debug.Log("setting image for polaroid: " + winfieldID   );
        AssetProvider ap = GetComponent<AssetProvider>();
    
        ap.WinfieldID = winfieldID;

        //VPD.currentWinCLip = ap.Clips[winfieldID];
        Texture2D WinPic = ap.PolaroidImages[winfieldID];
        StaticExample.CurrentWinfieldID = winfieldID;
        PolaroidImage.sprite = Sprite.Create(WinPic, new Rect(0, 0, WinPic.width, WinPic.height), new Vector2());
        //VPD.TimeOffsetForMulticontrollerPlay = VPD.TimeOffsetForVideoPlay + ap.MovieDurationsTillBlend[winfieldID];
        //VPD.TimeDisableVideoImage = VPD.TimeOffsetForVideoPlay + ap.MovieDurationsTillBlend[winfieldID]+ap.BlendTime;

    }


}
