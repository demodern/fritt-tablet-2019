﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class VideoPlayerDelay : MonoBehaviour {

    public FotoMachenController fmc;
    public float TimeOffsetForVideoPlay;
    public float TimeOffsetForMulticontrollerPlay;
    public float TimeDisableVideoImage;
    public VideoClip currentWinCLip;

    float currentTime = 0f;
    bool VideoIsRunning = false;
    bool MulticontrollerIsRunning = false;
    public Image VideoImage;
    bool VideoSequenceStarted = false;
    VideoPlayer vp;

    public float currentTimeDebug;


    // Use this for initialization
    void Start () {
        // VideoImage.enabled = true;
       
        VideoImage.enabled = false;
        vp = GetComponent<VideoPlayer>();
        vp.loopPointReached += EndReached;
        // vp.prepareCompleted += PlayVideoNow;

    }

    private void EndReached(VideoPlayer source)
    {
        Debug.Log("Videoplayer Stoped");
        source.Stop();
    }

    // Update is called once per frame
    void Update () {
        if (VideoSequenceStarted)
        {

            currentTime += Time.deltaTime;

            if (currentTime >= TimeOffsetForVideoPlay && !VideoIsRunning)
            {
                VideoIsRunning = true;
               PlayVideo();
            }
            if (currentTime >= TimeOffsetForMulticontrollerPlay && !MulticontrollerIsRunning)
            {
                MulticontrollerIsRunning = true;

                StartMultiController();

            }
            if (currentTime >= TimeDisableVideoImage )
            {
                VideoImage.enabled = false;

            }

            
        }

        currentTimeDebug = (float)vp.time;
    }

    public void StartVideoSequence()
    {
        //Debug.Log("StartVideoSequence called");
        VideoSequenceStarted = true;

        currentTime = 0f;
        VideoIsRunning = false;
        MulticontrollerIsRunning = false;
       // PrepareNewVideo();
    }


    void StartMultiController()
    {
        fmc.gameObject.SetActive(true);
        fmc.StartFotoMachen();
        //VideoImage.enabled = false;
    }



    void PlayVideo()
    {
        //Debug.Log("PlayVideo called");
        VideoImage.enabled = true;
        //VideoPlayer vpr = GetComponent<VideoPlayer>();
       // vp.clip = currentWinCLip;
        //vp.Prepare();
       



        vp.Play();
        
    }

    public void SetVideoForPlayer(VideoClip vc)
    {
        GetComponent<VideoPlayer>().clip = vc;
    }

    public void PlayVideoNow(UnityEngine.Video.VideoPlayer vPlayer)
    {
        VideoImage.enabled = true;
        vPlayer.Play();
        Debug.Log("VideoIsPrepared called");
    }
}
