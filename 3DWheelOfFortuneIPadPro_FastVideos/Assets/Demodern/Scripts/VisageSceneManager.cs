﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisageSceneManager : MonoBehaviour {

    public GameObject[] VisageObjects;
    public VisageManager VM;
    public ScreenShotManager ssm;
    //public Tracker visageTracker;
    public GameObject ARKitObjects;
    public GameObject ScreenmanagerCanvas;
    public PrintGenerator pg;

    bool PhotoCountdownIsRunning = false;
    float countdownTime = 0f;
    public float MaxCountdownTime = 4f;
    public GameObject[] FaceFilterGraphics;
    bool showFaceFilterGraphics = false;
    public MaskInterface mi;
    public FotoFrameAnimator ffa;

	// Use this for initialization
	void Start () {
        DeActivateVisage();
        ssm.PhotoIsDone += PhotoIsDone;
        //pg.PrintReady += PhotoIsDone;
        //visageTracker.onButtonPlay();
        
        //visageTracker.SetPlay();
        ffa.InitStartTransformations();
    }
	
	// Update is called once per frame
	void Update () {

        // Making the Photo is now in Canvas Countdown->Background_PhotoMaker !!!!
        /*
        if (PhotoCountdownIsRunning)
        {
            countdownTime += Time.deltaTime;
            if (countdownTime >= MaxCountdownTime)
            {
                MakePhoto();
                PhotoCountdownIsRunning = false;
            }
        }*/

        SetFaceFilterActiveState();
        
    }

    void SetFaceFilterActiveState() {
        foreach (GameObject go in FaceFilterGraphics)
        {

            go.SetActive(showFaceFilterGraphics);
        }
    }

    public void ActivateVisage() {
        SetObjectState(true);

        //visageTracker.SetPlay();
        ARKitObjects.SetActive(true);
        ffa.InitStartTransformations();
        //visageTracker.enabled = true;
        showFaceFilterGraphics = true;
        mi.SetMaskActiveByID(StaticExample.CurrentWinfieldID);
    }

    public void DeActivateVisage()
    {
        ARKitObjects.SetActive(false);
        SetObjectState(false);
        //visageTracker.SetPause();
        //visageTracker.enabled = false;
        showFaceFilterGraphics = false;

    }


    void SetObjectState(bool State)
    {
        foreach (GameObject go in VisageObjects)
        {
            go.SetActive(State);
        }
    }



    public void StartPhotoCountdown()
    {
        countdownTime = 0f;
        PhotoCountdownIsRunning = true;
        ScreenmanagerCanvas.SetActive(true);
    }

    void MakePhoto()
    {
        ssm.Photo();
    }

    public void PhotoIsDone()
    {
        VM.SetQRImage();
        VM.PhotoIsDone();
       
        ScreenmanagerCanvas.SetActive(false);
    }

    public void UploadAndSavePhoto()
    {
        ssm.UploadAndSaveOnDisk();
    }
}
