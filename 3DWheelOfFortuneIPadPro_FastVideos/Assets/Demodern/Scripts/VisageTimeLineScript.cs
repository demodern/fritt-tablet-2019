﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VisageTimeLineScript : MonoBehaviour {

	// Use this for initialization

	public ScreenShotManager SM;
	public VideoPlayer VC;
	public string SceneName;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void BackToWheelOfFortune(){
		SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
	}

	public void StartCountdown(){
		VC.Play ();
	}

	public void MakePhoto(){
		SM.Photo ();
	}


}
