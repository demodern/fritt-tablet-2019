﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlanetInteraction : MonoBehaviour 
{

    public delegate void WheelStoppedTurning();
    public WheelStoppedTurning wheelStopped;
    public float rotSpeed = 6;
	public float FrictionValue = 100;

    float frictionTMP = 0f;
	public float RotationThreshold = 0.1f;
	public Animator Wining;
    public Winindicator winindicator;
    public GameLogic GL;
    //	float prevRotX;
    //	float prevRotY;

    public float initialSpeedX = 0.1f;
    public float initialSpeedY = 0.05f;

    float rotX = 0.1f;
	float rotY = 0.05f;
    float rotX1 = 0.1f;
    float rotY1 = 0.05f;
    float rotX2 = 0.1f;
    float rotY2 = 0.05f;

    public bool isWon = false;

	public Slider FrictionSlider;
	public Text FrictionText;

	public Slider SpeedSlider;
	public Text SpeedText;

    public bool UserTouchIsActive = false;
    public bool UseAutorotate = true;

    bool UseDevelopeRotation = false;

//	bool WheelIsSpinning;

    float rotXMax = 0f;
    float rotYMax = 0f;

    float rotationFrictionTime = 0;
    public AnimationCurve RotationFriction;

    Vector2 prevTouch;
    void Start(){
//		WheelIsSpinning = false;
		isWon = true;
        UseAutorotate = true;


     rotX = initialSpeedX;
     rotY = initialSpeedY;
     rotX1 = initialSpeedX;
     rotY1 = initialSpeedY;
     rotX2 = initialSpeedX;
     rotY2 = initialSpeedY;
     frictionTMP = FrictionValue;

    

}

    public void SetIdleInteraction()
    {
        FrictionValue = frictionTMP;
    }

    public void SetIdleNoInteraction()
    {
       // FrictionValue = 0f;
    }

	void OnMouseDrag()
	{
        rotationFrictionTime = 0;
        // Debug.Log("OnMouseDrag called");
        //rotX = Input.GetAxis("Mouse X")*rotSpeed*Mathf.Deg2Rad;
        //rotY = Input.GetAxis("Mouse Y")*rotSpeed*Mathf.Deg2Rad;


        //rotX = Input.GetAxis("Mouse X")*rotSpeed*Mathf.Deg2Rad;
        //rotY = Input.GetAxis("Mouse Y")*rotSpeed*Mathf.Deg2Rad;
        if (UserTouchIsActive)
        {

            float resX = ((Input.mousePosition.x - prevTouch.x) * Time.deltaTime) / Screen.width ;
            float resY = ((Input.mousePosition.y - prevTouch.y) * Time.deltaTime) / Screen.height ;

            float deltaX = (Input.mousePosition.x - prevTouch.x) * Time.deltaTime ;
            float deltaY = (Input.mousePosition.y - prevTouch.y) * Time.deltaTime;


            float xxx = Input.mousePosition.x - prevTouch.x;
           
          
            rotX = resX * (rotSpeed * Mathf.Deg2Rad * Time.deltaTime);
            rotY = resY * (rotSpeed * Mathf.Deg2Rad * Time.deltaTime);

            rotX = deltaX * rotSpeed;
            rotY = deltaY * rotSpeed;
          // Debug.Log("rotX without friction:" + rotX);
            if (rotXMax* rotXMax < rotX * rotX)
            {
                rotXMax = rotX;
            }
            if (rotYMax* rotYMax < rotY * rotY)
            {
                rotYMax = rotY;
            }

            if (!UseDevelopeRotation)
            {
                rotX = rotXMax;
                rotY = rotYMax;
            }


           
           // Debug.Log("rotX with friction:" + rotX);
            prevTouch = Input.mousePosition;
        }
    }

    public void ToggleSpinMode()
    {
        if (UseDevelopeRotation)
        {
            UseDevelopeRotation = false;
        }
        else
        {
            UseDevelopeRotation = true;
        }
        Debug.Log("use development:" + UseDevelopeRotation);
       
    }

    void OnMouseDown(){
        if (UserTouchIsActive)
        {
            Debug.Log("OnMouseDown");
            winindicator.SetAllFieldsToDark();
            prevTouch = Input.mousePosition;
            GL.HideJetztDrehen();
            GL.ActivateWinfieldCollision();
             rotXMax = 0f;
             rotYMax = 0f;
        }
    }

	void OnMouseUp()
	{
        if (UserTouchIsActive)
        {
            // If your mouse hovers over the GameObject with the script attached, output this message
            // Debug.Log("Drag ended!");



            isWon = false; 
           // GetComponent<EasyFXAudioPlayer>().PlayAudioClip();

            
            GL.SetPlanetInteractionState(false);    
            
            //prevTouch = Input.mousePosition;
        }
    }



    public void ResetInteractionState()
    {
        //rotX = 0.1f;
        //rotY = 0.05f;
       // Debug.Log("ResetInteractionState called");
        isWon = true;
    }

    public void SetFriction(){
		//FrictionValue = FrictionSlider.value;
		//FrictionText.text = FrictionValue.ToString ();	
	}

	public void SetSpeed(){
		rotSpeed = SpeedSlider.value;
		//SpeedText.text = rotSpeed.ToString ();	
	}


	void Update(){
       // rotX = rotX - rotX*(FrictionValue * Time.deltaTime);
        //rotY = rotY - rotY*(FrictionValue * Time.deltaTime);

        float finalX = RotationFriction.Evaluate(rotationFrictionTime)* rotX;
        float finalY = RotationFriction.Evaluate(rotationFrictionTime) * rotY;
        if (UseAutorotate)
            {
            rotationFrictionTime = 0f;
            finalX = 0.001f;
            finalY = 0.0005f;
            rotX = finalX;
            rotY = finalY;
        }



            transform.RotateAround(Vector3.up, -finalX);
            transform.RotateAround(Vector3.right, finalY);
        
       

        if (Mathf.Abs (finalX)+ Mathf.Abs(finalY) <= RotationThreshold  ) {
			rotX = 0;
			rotY = 0;
			if(isWon == false){
			ShowWin ();
            isWon = true;
            }
		}
        rotationFrictionTime += Time.deltaTime/FrictionValue;
        //Debug.Log("rotation friction time: " + rotationFrictionTime);

    }


	public void ShowWin(){
	Debug.Log ("You Win");
        //GL.SpinnIsFinished();
       
        /*
            Wining.SetTrigger ("Win");
            */
         wheelStopped(); 
        gameObject.GetComponent<PlanetInteraction>().enabled = false;
    }

    public void ActivatePlanetInteraction()
    {
        gameObject.GetComponent<PlanetInteraction>().enabled = true;
    }

    public void TriggerWin(){
		//Wining.SetTrigger ("Win");
	}
}