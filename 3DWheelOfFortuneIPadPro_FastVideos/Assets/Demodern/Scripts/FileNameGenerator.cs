﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileNameGenerator
{

    static int counter = 0;

    public static string GenerateFileName(string prefix, string suffix = "")
    {

        string rndHash = DateTime.Now.GetHashCode().ToString();
        string name = prefix + "_" + rndHash + "_" + counter.ToString() + suffix;

        counter++;
        return name;

    }
}
