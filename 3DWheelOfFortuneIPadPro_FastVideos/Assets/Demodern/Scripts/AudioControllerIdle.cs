﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerIdle : MonoBehaviour {

    public float FadeTime;
    public float fadeDelayStart = 0f;
    public float fadeDelayEnd = 0f;
    public EasyFXMultiController efm;
    AudioSource aSource;
    float currentTime = 0f;

    float TargetVolume = 0;

    bool NeedsUpdate = false;
    float startVolume = 0F;
    float CurrentVolume = 0f;
	// Use this for initialization
	void Start () {
        efm.StartTriggered += FadeIn;
        efm.MenuEndFinished += FadeOut;

    }
	
	// Update is called once per frame
	void Update () {

        if (NeedsUpdate && aSource !=  null)
        {
            currentTime += Time.deltaTime/ FadeTime;
            if (currentTime >1f)
            {
                currentTime = 1f;
                NeedsUpdate = false;
            }
           
            CurrentVolume =  Mathf.Lerp(startVolume, TargetVolume, currentTime);
 
            aSource.volume = CurrentVolume;
        }

		
	}

    public void FadeIn()
    {
        FadeTo(1f, fadeDelayStart);
        Debug.Log("Fade In Called");
    }

    public void FadeOut()
    {
        FadeTo(0f, fadeDelayEnd);
    }

    void FadeTo(float targetVolume, float Offset)
    {
        TargetVolume = targetVolume;
        currentTime = -Offset;
        if (aSource == null)
        {
            aSource = GetComponent<AudioSource>();
            aSource.loop = true;
            aSource.Play();
            
        }
        startVolume = aSource.volume;
        NeedsUpdate = true;

    }


}
