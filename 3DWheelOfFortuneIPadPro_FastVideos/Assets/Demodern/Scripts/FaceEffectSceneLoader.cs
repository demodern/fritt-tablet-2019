﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FaceEffectSceneLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
        LoadFaceFilterScene();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void LoadFaceFilterScene(){
        SceneManager.LoadScene("Demodern/Scenes/ARKitVersion/MainVisage4_FaceFilter ARKit", LoadSceneMode.Additive);
        SceneManager.LoadScene("Demodern/Scenes/ARKitVersion/UserFlowClean ARKit", LoadSceneMode.Additive);
    }
}
